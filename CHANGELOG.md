# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.0.73](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.72...v4.0.73) (2025-03-08)


### Bug Fixes

* **deps:** lock file maintenance ([770fd8d](https://github.com/scratchfoundation/scratch-storage/commit/770fd8d4a268640b5c439b8546f9a439be4972ec))

## [4.0.72](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.71...v4.0.72) (2025-03-07)


### Bug Fixes

* **deps:** lock file maintenance ([1899b3c](https://github.com/scratchfoundation/scratch-storage/commit/1899b3cc95f4312af64f8230d0e4c1b666f96b50))

## [4.0.71](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.70...v4.0.71) (2025-03-07)


### Bug Fixes

* **deps:** lock file maintenance ([38f88c9](https://github.com/scratchfoundation/scratch-storage/commit/38f88c98a4054dc2cdef26fcdfff8b0b0b0cba9f))

## [4.0.70](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.69...v4.0.70) (2025-03-06)


### Bug Fixes

* **deps:** lock file maintenance ([b9ff501](https://github.com/scratchfoundation/scratch-storage/commit/b9ff50126477ad7ef9023b63f19c78dcb20f0dd4))

## [4.0.69](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.68...v4.0.69) (2025-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([6395bb4](https://github.com/scratchfoundation/scratch-storage/commit/6395bb402560f2130679b389b9ea7c4ea208e46b))

## [4.0.68](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.67...v4.0.68) (2025-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([6ceb24c](https://github.com/scratchfoundation/scratch-storage/commit/6ceb24c717f0df74a498cbf5d909f3af0aad4645))

## [4.0.67](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.66...v4.0.67) (2025-03-04)


### Bug Fixes

* **deps:** lock file maintenance ([3db1e69](https://github.com/scratchfoundation/scratch-storage/commit/3db1e698b8ceaea4ed8c3135042dd0f5f7d6bf5f))

## [4.0.66](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.65...v4.0.66) (2025-03-03)


### Bug Fixes

* **deps:** lock file maintenance ([f5091bd](https://github.com/scratchfoundation/scratch-storage/commit/f5091bd36b48a551b15c3912eb69fb1353d1519b))

## [4.0.65](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.64...v4.0.65) (2025-03-02)


### Bug Fixes

* **deps:** lock file maintenance ([197235b](https://github.com/scratchfoundation/scratch-storage/commit/197235b62271141882f0b45ac207993552d46fb3))

## [4.0.64](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.63...v4.0.64) (2025-03-01)


### Bug Fixes

* **deps:** lock file maintenance ([8982b38](https://github.com/scratchfoundation/scratch-storage/commit/8982b38578268498940a4cb49051470d70ae8c83))

## [4.0.63](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.62...v4.0.63) (2025-02-28)


### Bug Fixes

* **deps:** lock file maintenance ([e54f762](https://github.com/scratchfoundation/scratch-storage/commit/e54f762471fa7dfc1af9cc3666ed793f7f3716a2))

## [4.0.62](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.61...v4.0.62) (2025-02-28)


### Bug Fixes

* **deps:** lock file maintenance ([774fd6d](https://github.com/scratchfoundation/scratch-storage/commit/774fd6d8bb47f8ecb0a2abfe2348435933e6be79))

## [4.0.61](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.60...v4.0.61) (2025-02-27)


### Bug Fixes

* **deps:** lock file maintenance ([5bef570](https://github.com/scratchfoundation/scratch-storage/commit/5bef570e2899f3b8e3d520f7ca065832e1a296e6))

## [4.0.60](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.59...v4.0.60) (2025-02-26)


### Bug Fixes

* **deps:** lock file maintenance ([5664010](https://github.com/scratchfoundation/scratch-storage/commit/566401094f5d6c1090f68def9936bf2b3d9746d1))

## [4.0.59](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.58...v4.0.59) (2025-02-25)


### Bug Fixes

* **deps:** lock file maintenance ([c998427](https://github.com/scratchfoundation/scratch-storage/commit/c9984271e1e36119614dd796134f9ccaac91edd2))

## [4.0.58](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.57...v4.0.58) (2025-02-24)


### Bug Fixes

* **deps:** lock file maintenance ([d3070cd](https://github.com/scratchfoundation/scratch-storage/commit/d3070cd9c047bfe6dd5902de4c203410e6a311ab))

## [4.0.57](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.56...v4.0.57) (2025-02-23)


### Bug Fixes

* **deps:** lock file maintenance ([fc71960](https://github.com/scratchfoundation/scratch-storage/commit/fc71960e39d67a5bb6e337c8fc55c5af66e5c0ea))

## [4.0.56](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.55...v4.0.56) (2025-02-20)


### Bug Fixes

* **deps:** lock file maintenance ([f83a719](https://github.com/scratchfoundation/scratch-storage/commit/f83a719cc9e7e75ef6462e7e364811c4936c8b9c))

## [4.0.55](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.54...v4.0.55) (2025-02-19)


### Bug Fixes

* **deps:** lock file maintenance ([8125ad7](https://github.com/scratchfoundation/scratch-storage/commit/8125ad7eb2b0caa43fc2c954a51697a01715676c))

## [4.0.54](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.53...v4.0.54) (2025-02-18)


### Bug Fixes

* **deps:** lock file maintenance ([f829c07](https://github.com/scratchfoundation/scratch-storage/commit/f829c07c6973f6b64b5b82704894062786d0866e))

## [4.0.53](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.52...v4.0.53) (2025-02-17)


### Bug Fixes

* **deps:** lock file maintenance ([553fb0d](https://github.com/scratchfoundation/scratch-storage/commit/553fb0d9002f3ebccb87bf536b018e4d6a5f59a7))

## [4.0.52](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.51...v4.0.52) (2025-02-15)


### Bug Fixes

* **deps:** lock file maintenance ([aac41fc](https://github.com/scratchfoundation/scratch-storage/commit/aac41fca701d125f17a78715c6471364d266763b))

## [4.0.51](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.50...v4.0.51) (2025-02-15)


### Bug Fixes

* **deps:** lock file maintenance ([3358cde](https://github.com/scratchfoundation/scratch-storage/commit/3358cde7ae5e3fb4ae2c0a3796454cc1aca11cd6))

## [4.0.50](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.49...v4.0.50) (2025-02-12)


### Bug Fixes

* **deps:** lock file maintenance ([39b6158](https://github.com/scratchfoundation/scratch-storage/commit/39b61580c132d9b3238f2b5d9cd76ac4dd114c5e))

## [4.0.49](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.48...v4.0.49) (2025-02-09)


### Bug Fixes

* **deps:** lock file maintenance ([d0bc217](https://github.com/scratchfoundation/scratch-storage/commit/d0bc2174cc7180edfd8b6cede06e168ad3913c58))

## [4.0.48](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.47...v4.0.48) (2025-02-08)


### Bug Fixes

* **deps:** lock file maintenance ([7cf25e7](https://github.com/scratchfoundation/scratch-storage/commit/7cf25e72d392d030ad6dee7aa23a5bdd0752c0da))

## [4.0.47](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.46...v4.0.47) (2025-02-07)


### Bug Fixes

* **deps:** lock file maintenance ([3e9f7ac](https://github.com/scratchfoundation/scratch-storage/commit/3e9f7acbfa3e8d3e62e2b970a0c3f6f40cff80a6))

## [4.0.46](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.45...v4.0.46) (2025-02-07)


### Bug Fixes

* **deps:** lock file maintenance ([975d670](https://github.com/scratchfoundation/scratch-storage/commit/975d670d8f8eaaf6f65c4ec5f04d4e9f15d9f42c))

## [4.0.45](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.44...v4.0.45) (2025-02-06)


### Bug Fixes

* **deps:** lock file maintenance ([0f96f4d](https://github.com/scratchfoundation/scratch-storage/commit/0f96f4d83bdef9cb3b29a250917fa3b4b4f1400a))

## [4.0.44](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.43...v4.0.44) (2025-02-05)


### Bug Fixes

* **deps:** lock file maintenance ([0207ee0](https://github.com/scratchfoundation/scratch-storage/commit/0207ee0ae17d2f5114d16acfeed9c2b1476c3a9b))

## [4.0.43](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.42...v4.0.43) (2025-02-04)


### Bug Fixes

* **deps:** lock file maintenance ([f083b29](https://github.com/scratchfoundation/scratch-storage/commit/f083b299ebde2b995603f050b82f7d8c65d30b96))

## [4.0.42](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.41...v4.0.42) (2025-02-03)


### Bug Fixes

* **deps:** lock file maintenance ([2808aea](https://github.com/scratchfoundation/scratch-storage/commit/2808aeae0a795a3ebd84a3cde7acb791bdfd3c6e))

## [4.0.41](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.40...v4.0.41) (2025-02-02)


### Bug Fixes

* **deps:** lock file maintenance ([28004d8](https://github.com/scratchfoundation/scratch-storage/commit/28004d87258a16c99c3037d233df3bdbafe4ef01))

## [4.0.40](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.39...v4.0.40) (2025-01-30)


### Bug Fixes

* **deps:** lock file maintenance ([4cedcb4](https://github.com/scratchfoundation/scratch-storage/commit/4cedcb481b16f5e59666d2c02151393b716bfcb8))

## [4.0.39](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.38...v4.0.39) (2025-01-26)


### Bug Fixes

* **deps:** lock file maintenance ([a0676b2](https://github.com/scratchfoundation/scratch-storage/commit/a0676b29d182024ba3238cd52da4d4a8fc67def6))

## [4.0.38](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.37...v4.0.38) (2025-01-25)


### Bug Fixes

* **deps:** lock file maintenance ([d42a467](https://github.com/scratchfoundation/scratch-storage/commit/d42a467b9c41ad59dc2b03d593b97403b309e2b2))

## [4.0.37](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.36...v4.0.37) (2025-01-24)


### Bug Fixes

* **deps:** lock file maintenance ([1821af3](https://github.com/scratchfoundation/scratch-storage/commit/1821af3e3fef715baefab9ad225bb84fd126b33f))

## [4.0.36](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.35...v4.0.36) (2025-01-23)


### Bug Fixes

* **deps:** lock file maintenance ([35a4d67](https://github.com/scratchfoundation/scratch-storage/commit/35a4d6791fdac1779128d8e383c5845fdff0b3d9))

## [4.0.35](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.34...v4.0.35) (2025-01-21)


### Bug Fixes

* **deps:** lock file maintenance ([bff1c6c](https://github.com/scratchfoundation/scratch-storage/commit/bff1c6c6b64120ffed30e2dfee43981db72c8214))

## [4.0.34](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.33...v4.0.34) (2025-01-20)


### Bug Fixes

* **deps:** lock file maintenance ([fd27040](https://github.com/scratchfoundation/scratch-storage/commit/fd27040054991abeb71d26a695bbbda4debeb271))

## [4.0.33](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.32...v4.0.33) (2025-01-19)


### Bug Fixes

* **deps:** lock file maintenance ([62bb60b](https://github.com/scratchfoundation/scratch-storage/commit/62bb60bab4256fe434d90ed87561de243a52ca70))

## [4.0.32](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.31...v4.0.32) (2025-01-16)


### Bug Fixes

* **deps:** lock file maintenance ([f087dd6](https://github.com/scratchfoundation/scratch-storage/commit/f087dd697133aaa8bbde142bfec6bb92136aff61))

## [4.0.31](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.30...v4.0.31) (2025-01-15)


### Bug Fixes

* **deps:** lock file maintenance ([88e78f0](https://github.com/scratchfoundation/scratch-storage/commit/88e78f0e44f717be824c829cfd193ec42fad7a4a))

## [4.0.30](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.29...v4.0.30) (2025-01-14)


### Bug Fixes

* **deps:** lock file maintenance ([26eca2e](https://github.com/scratchfoundation/scratch-storage/commit/26eca2e13e4ce5fb9acb4828013952d846e89959))

## [4.0.29](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.28...v4.0.29) (2025-01-11)


### Bug Fixes

* **deps:** lock file maintenance ([b97bd0a](https://github.com/scratchfoundation/scratch-storage/commit/b97bd0a7851b4d84b61d1bc1490eaa7b2dd9a71f))

## [4.0.28](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.27...v4.0.28) (2025-01-11)


### Bug Fixes

* **deps:** lock file maintenance ([f681740](https://github.com/scratchfoundation/scratch-storage/commit/f6817401d6b17f046c9b2e1df22d50c38cf3f9cc))

## [4.0.27](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.26...v4.0.27) (2025-01-10)


### Bug Fixes

* **deps:** lock file maintenance ([7f4a775](https://github.com/scratchfoundation/scratch-storage/commit/7f4a775966e2a16692be908e51874bccf53db904))

## [4.0.26](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.25...v4.0.26) (2025-01-09)


### Bug Fixes

* **deps:** lock file maintenance ([42a9478](https://github.com/scratchfoundation/scratch-storage/commit/42a9478e16124dccbb4c24479dd6686ef10f6521))

## [4.0.25](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.24...v4.0.25) (2025-01-09)


### Bug Fixes

* **deps:** lock file maintenance ([e0b29ae](https://github.com/scratchfoundation/scratch-storage/commit/e0b29aeb85aae2b2dde072d8dbec3026b145d364))

## [4.0.24](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.23...v4.0.24) (2025-01-07)


### Bug Fixes

* error calling fetch ([650fca5](https://github.com/scratchfoundation/scratch-storage/commit/650fca5ecdd3eb7bdeab6c1ee80e8bb634fa0b59))
* fix failing specs ([41816e4](https://github.com/scratchfoundation/scratch-storage/commit/41816e448c082a78d1572c0c788eb5f64ff6bd3a))

## [4.0.23](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.22...v4.0.23) (2024-12-20)


### Bug Fixes

* **deps:** buffer is a runtime dependency ([3616355](https://github.com/scratchfoundation/scratch-storage/commit/361635522767fdbbf2816e9ddc943bdd2d59fca4))

## [4.0.22](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.21...v4.0.22) (2024-12-10)


### Bug Fixes

* **deps:** lock file maintenance ([ca84557](https://github.com/scratchfoundation/scratch-storage/commit/ca84557ec72679bd039fc315d38a6a63e4ee431b))

## [4.0.21](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.20...v4.0.21) (2024-12-08)


### Bug Fixes

* **deps:** lock file maintenance ([578d4e0](https://github.com/scratchfoundation/scratch-storage/commit/578d4e0166ff0c64220735d375ad80a32b01f863))

## [4.0.20](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.19...v4.0.20) (2024-12-08)


### Bug Fixes

* **deps:** lock file maintenance ([1ec15b8](https://github.com/scratchfoundation/scratch-storage/commit/1ec15b823c8c403770efd5ab43815990fc66bb74))

## [4.0.19](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.18...v4.0.19) (2024-12-07)


### Bug Fixes

* **deps:** lock file maintenance ([eda0fbb](https://github.com/scratchfoundation/scratch-storage/commit/eda0fbb26f7c45fa46609650b787586dff510a7d))

## [4.0.18](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.17...v4.0.18) (2024-12-07)


### Bug Fixes

* **deps:** lock file maintenance ([4be8838](https://github.com/scratchfoundation/scratch-storage/commit/4be8838f76930f3ec06411874c3c1cd8b56a4a3c))

## [4.0.17](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.16...v4.0.17) (2024-12-06)


### Bug Fixes

* **deps:** lock file maintenance ([8796cfe](https://github.com/scratchfoundation/scratch-storage/commit/8796cfe7de12436a740bae60881e4f2f8fa26c8d))

## [4.0.16](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.15...v4.0.16) (2024-12-06)


### Bug Fixes

* **deps:** lock file maintenance ([7b87b00](https://github.com/scratchfoundation/scratch-storage/commit/7b87b00c181f62124d236e96f388b87b280b9053))

## [4.0.15](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.14...v4.0.15) (2024-12-05)


### Bug Fixes

* **deps:** lock file maintenance ([62c718d](https://github.com/scratchfoundation/scratch-storage/commit/62c718d08a1542d06c4533502dc22fa8f781f0e6))

## [4.0.14](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.13...v4.0.14) (2024-12-05)


### Bug Fixes

* **deps:** lock file maintenance ([2e49714](https://github.com/scratchfoundation/scratch-storage/commit/2e49714cd10db235b98b85136059ff8c5c36cf70))

## [4.0.13](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.12...v4.0.13) (2024-12-03)


### Bug Fixes

* **deps:** lock file maintenance ([4b97a45](https://github.com/scratchfoundation/scratch-storage/commit/4b97a455b08c878e786f8e5ee4e44b609d00c80f))

## [4.0.12](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.11...v4.0.12) (2024-12-03)


### Bug Fixes

* **deps:** lock file maintenance ([9009a2d](https://github.com/scratchfoundation/scratch-storage/commit/9009a2db419b9e336d643c5604d0035d289e0d5b))

## [4.0.11](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.10...v4.0.11) (2024-12-02)


### Bug Fixes

* **deps:** lock file maintenance ([99c7062](https://github.com/scratchfoundation/scratch-storage/commit/99c7062afb7bd7ccf5f17c68c2c609d98ec0100a))

## [4.0.10](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.9...v4.0.10) (2024-12-01)


### Bug Fixes

* **deps:** lock file maintenance ([a69ccd9](https://github.com/scratchfoundation/scratch-storage/commit/a69ccd98b7f9ab090cbb56e165c4ac1e82ae9867))

## [4.0.9](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.8...v4.0.9) (2024-11-30)


### Bug Fixes

* **deps:** lock file maintenance ([6e25506](https://github.com/scratchfoundation/scratch-storage/commit/6e25506b7cf1aadf404d3bde72d742f59405be7c))

## [4.0.8](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.7...v4.0.8) (2024-11-29)


### Bug Fixes

* **deps:** lock file maintenance ([76bdb31](https://github.com/scratchfoundation/scratch-storage/commit/76bdb31ec27d02d3ee11ec20d7fed457b9f83fe2))

## [4.0.7](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.6...v4.0.7) (2024-11-29)


### Bug Fixes

* **deps:** lock file maintenance ([7e27f22](https://github.com/scratchfoundation/scratch-storage/commit/7e27f22404f53fa4b0b84917746943bc0a5bd406))

## [4.0.6](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.5...v4.0.6) (2024-11-28)


### Bug Fixes

* **deps:** lock file maintenance ([03200bc](https://github.com/scratchfoundation/scratch-storage/commit/03200bc52a1709dd1eefb65f83eb534f89ce9d0d))

## [4.0.5](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.4...v4.0.5) (2024-11-28)


### Bug Fixes

* **deps:** lock file maintenance ([f02292c](https://github.com/scratchfoundation/scratch-storage/commit/f02292c51ecd64af5f6d2ac38a35b7d53ab4f7e7))

## [4.0.4](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.3...v4.0.4) (2024-11-27)


### Bug Fixes

* **deps:** lock file maintenance ([183b243](https://github.com/scratchfoundation/scratch-storage/commit/183b243deaf0f8b4e6f09d1b19c11669edba4bed))

## [4.0.3](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.2...v4.0.3) (2024-11-27)


### Bug Fixes

* **deps:** lock file maintenance ([83dac35](https://github.com/scratchfoundation/scratch-storage/commit/83dac353084b414966b7bffefdeedf1a8fe73bc2))

## [4.0.2](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.1...v4.0.2) (2024-11-26)


### Bug Fixes

* **deps:** lock file maintenance ([7839068](https://github.com/scratchfoundation/scratch-storage/commit/783906847c2a9b3a41dcda4f38dc1ccaaf7a9f61))

## [4.0.1](https://github.com/scratchfoundation/scratch-storage/compare/v4.0.0...v4.0.1) (2024-11-26)


### Bug Fixes

* **deps:** lock file maintenance ([57e6e28](https://github.com/scratchfoundation/scratch-storage/commit/57e6e28e8ff8cb1c0c4843dc09aba6e8a0aea6b4))

# [4.0.0](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.39...v4.0.0) (2024-11-25)


* chore!: set license to AGPL-3.0-only ([9de39ba](https://github.com/scratchfoundation/scratch-storage/commit/9de39bae84ed3eccc24f1b60cb51e822b20f160d))


### BREAKING CHANGES

* This project is now licensed under the AGPL version 3.0

See https://www.scratchfoundation.org/open-source-license

## [3.0.39](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.38...v3.0.39) (2024-11-25)


### Bug Fixes

* **deps:** lock file maintenance ([5d66405](https://github.com/scratchfoundation/scratch-storage/commit/5d6640504763b4a9dde973fda6da7108b99e166a))

## [3.0.38](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.37...v3.0.38) (2024-11-24)


### Bug Fixes

* **deps:** lock file maintenance ([4dd626a](https://github.com/scratchfoundation/scratch-storage/commit/4dd626a89ab027e7bd0fb7c07803103c000fbc92))

## [3.0.37](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.36...v3.0.37) (2024-11-23)


### Bug Fixes

* **deps:** lock file maintenance ([c4c6d08](https://github.com/scratchfoundation/scratch-storage/commit/c4c6d08935337cb0bff54e11cec8465c98a1685b))

## [3.0.36](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.35...v3.0.36) (2024-11-23)


### Bug Fixes

* **deps:** lock file maintenance ([b3f8874](https://github.com/scratchfoundation/scratch-storage/commit/b3f88746cc11b8e7886982350045581db7cc71f6))

## [3.0.35](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.34...v3.0.35) (2024-11-22)


### Bug Fixes

* **deps:** lock file maintenance ([b14083f](https://github.com/scratchfoundation/scratch-storage/commit/b14083fe4ef615f95295c75cf2e0bc8599b1baf1))

## [3.0.34](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.33...v3.0.34) (2024-11-22)


### Bug Fixes

* **deps:** lock file maintenance ([37bf128](https://github.com/scratchfoundation/scratch-storage/commit/37bf128ae31cf4844c5678eb49542041429b5064))

## [3.0.33](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.32...v3.0.33) (2024-11-20)


### Bug Fixes

* **deps:** lock file maintenance ([40e57c7](https://github.com/scratchfoundation/scratch-storage/commit/40e57c7994ac25c2d2ed6241904c959db4964c19))

## [3.0.32](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.31...v3.0.32) (2024-11-19)


### Bug Fixes

* **deps:** lock file maintenance ([4e6e413](https://github.com/scratchfoundation/scratch-storage/commit/4e6e41375b5a5be5569a6371d8ec99e041619c7d))

## [3.0.31](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.30...v3.0.31) (2024-11-19)


### Bug Fixes

* **deps:** lock file maintenance ([a7f3295](https://github.com/scratchfoundation/scratch-storage/commit/a7f3295c91bf38fd33462617cb08451fe61b4f59))

## [3.0.30](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.29...v3.0.30) (2024-11-16)


### Bug Fixes

* **deps:** lock file maintenance ([d8b4850](https://github.com/scratchfoundation/scratch-storage/commit/d8b48507070a61b52cb4ccabad6d3274296afae3))

## [3.0.29](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.28...v3.0.29) (2024-11-16)


### Bug Fixes

* **deps:** lock file maintenance ([d238ddb](https://github.com/scratchfoundation/scratch-storage/commit/d238ddb0cbd4d41c70658d74a04ecf1579f21c51))

## [3.0.28](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.27...v3.0.28) (2024-11-15)


### Bug Fixes

* **deps:** lock file maintenance ([b8047b0](https://github.com/scratchfoundation/scratch-storage/commit/b8047b0e3747d16d08e120068aa321f36655d5d5))

## [3.0.27](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.26...v3.0.27) (2024-11-15)


### Bug Fixes

* **deps:** lock file maintenance ([009f378](https://github.com/scratchfoundation/scratch-storage/commit/009f37878e0dd6a0f2393c941fbafaea4b0e7d71))

## [3.0.26](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.25...v3.0.26) (2024-11-13)


### Bug Fixes

* **deps:** lock file maintenance ([543a85a](https://github.com/scratchfoundation/scratch-storage/commit/543a85a74964165cc25e57c703738d96d6c5795c))

## [3.0.25](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.24...v3.0.25) (2024-11-12)


### Bug Fixes

* **deps:** lock file maintenance ([a62ea10](https://github.com/scratchfoundation/scratch-storage/commit/a62ea104ffa67907d37abe41cf84ef9767b3a855))

## [3.0.24](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.23...v3.0.24) (2024-11-12)


### Bug Fixes

* **deps:** lock file maintenance ([fd8a5e2](https://github.com/scratchfoundation/scratch-storage/commit/fd8a5e2a759558c39fdcc373aff65411f822ff6f))

## [3.0.23](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.22...v3.0.23) (2024-11-11)


### Bug Fixes

* **deps:** lock file maintenance ([f0ac960](https://github.com/scratchfoundation/scratch-storage/commit/f0ac9607d28678bfa5d2c333e2ed81ee0466f617))

## [3.0.22](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.21...v3.0.22) (2024-11-11)


### Bug Fixes

* **deps:** lock file maintenance ([cb013c9](https://github.com/scratchfoundation/scratch-storage/commit/cb013c943c9ba5d42d64211715dfc06882ca745f))

## [3.0.21](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.20...v3.0.21) (2024-11-09)


### Bug Fixes

* **deps:** lock file maintenance ([baa3a1e](https://github.com/scratchfoundation/scratch-storage/commit/baa3a1ed034be927586165f04bef414b20b2f656))

## [3.0.20](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.19...v3.0.20) (2024-11-09)


### Bug Fixes

* **deps:** lock file maintenance ([1e8431e](https://github.com/scratchfoundation/scratch-storage/commit/1e8431ebea4de8246248d8f0c3dedb52f70a765a))

## [3.0.19](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.18...v3.0.19) (2024-11-08)


### Bug Fixes

* **deps:** lock file maintenance ([97827ae](https://github.com/scratchfoundation/scratch-storage/commit/97827aed4c73e8d5830c355d891de91cf98d0242))

## [3.0.18](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.17...v3.0.18) (2024-11-07)


### Bug Fixes

* **deps:** lock file maintenance ([cd70ae3](https://github.com/scratchfoundation/scratch-storage/commit/cd70ae3b6cc79a26503e35399cd18830e9c0adcb))

## [3.0.17](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.16...v3.0.17) (2024-11-07)


### Bug Fixes

* **deps:** lock file maintenance ([a2dda7f](https://github.com/scratchfoundation/scratch-storage/commit/a2dda7f3782884788bce275346cdbd9dc1e8561e))

## [3.0.16](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.15...v3.0.16) (2024-11-06)


### Bug Fixes

* **deps:** lock file maintenance ([fb5fdb6](https://github.com/scratchfoundation/scratch-storage/commit/fb5fdb6f2d5d550ff0db4b9a9b435bb0f66a4713))

## [3.0.15](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.14...v3.0.15) (2024-11-05)


### Bug Fixes

* **deps:** lock file maintenance ([64d6a51](https://github.com/scratchfoundation/scratch-storage/commit/64d6a51d587c47505f26a3a9142f8bc05fa098ad))

## [3.0.14](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.13...v3.0.14) (2024-11-05)


### Bug Fixes

* **deps:** lock file maintenance ([6de576e](https://github.com/scratchfoundation/scratch-storage/commit/6de576e7259ab0b85db83357198deb86cb589cf6))

## [3.0.13](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.12...v3.0.13) (2024-11-03)


### Bug Fixes

* **deps:** lock file maintenance ([bef3032](https://github.com/scratchfoundation/scratch-storage/commit/bef30324f6247d48d3cca072803a66e34972268b))

## [3.0.12](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.11...v3.0.12) (2024-11-03)


### Bug Fixes

* **deps:** lock file maintenance ([2de7ab6](https://github.com/scratchfoundation/scratch-storage/commit/2de7ab6abb78c56fb2ff95f87871ac0b96c6392c))

## [3.0.11](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.10...v3.0.11) (2024-11-01)


### Bug Fixes

* **deps:** lock file maintenance ([c38870f](https://github.com/scratchfoundation/scratch-storage/commit/c38870f6970d57b326b6272f3b57e70dc497d693))

## [3.0.10](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.9...v3.0.10) (2024-11-01)


### Bug Fixes

* **deps:** lock file maintenance ([4f4d9d4](https://github.com/scratchfoundation/scratch-storage/commit/4f4d9d4f1110287bf0b8db9525e04430bcdfb0a4))

## [3.0.9](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.8...v3.0.9) (2024-10-31)


### Bug Fixes

* **deps:** lock file maintenance ([f8e1407](https://github.com/scratchfoundation/scratch-storage/commit/f8e14075ed2be3b9debc6e9ac3341a690137dd07))

## [3.0.8](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.7...v3.0.8) (2024-10-31)


### Bug Fixes

* **deps:** lock file maintenance ([f96a40e](https://github.com/scratchfoundation/scratch-storage/commit/f96a40edf5d5ec097d36d1a3427e44a8e4b7bd2d))

## [3.0.7](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.6...v3.0.7) (2024-10-30)


### Bug Fixes

* **deps:** lock file maintenance ([1135b49](https://github.com/scratchfoundation/scratch-storage/commit/1135b493dc2bd26539dd80f5ef65ff72584f8b35))

## [3.0.6](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.5...v3.0.6) (2024-10-29)


### Bug Fixes

* **deps:** lock file maintenance ([6f9ebca](https://github.com/scratchfoundation/scratch-storage/commit/6f9ebca3b3211bd364fef9f481eb43d6cd525a22))

## [3.0.5](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.4...v3.0.5) (2024-10-28)


### Bug Fixes

* **deps:** lock file maintenance ([4b64f6c](https://github.com/scratchfoundation/scratch-storage/commit/4b64f6cdce009e0b99113d191daeeda9fa3d7bc4))

## [3.0.4](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.3...v3.0.4) (2024-10-27)


### Bug Fixes

* **deps:** lock file maintenance ([2f0ba80](https://github.com/scratchfoundation/scratch-storage/commit/2f0ba80631ef0986007705e31e4842e6785c5d89))

## [3.0.3](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.2...v3.0.3) (2024-10-26)


### Bug Fixes

* **deps:** lock file maintenance ([a218bd0](https://github.com/scratchfoundation/scratch-storage/commit/a218bd0476d0196d6a53f801abe0cc35931bf0a8))

## [3.0.2](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.1...v3.0.2) (2024-10-25)


### Bug Fixes

* **deps:** lock file maintenance ([edfd458](https://github.com/scratchfoundation/scratch-storage/commit/edfd458daa05dccf1fd858903b7bfb401f20ba6e))

## [3.0.1](https://github.com/scratchfoundation/scratch-storage/compare/v3.0.0...v3.0.1) (2024-10-23)


### Bug Fixes

* fix web worker usage in scratch-vm integration tests ([78b2c66](https://github.com/scratchfoundation/scratch-storage/commit/78b2c66140dbee8f8cbde9dc546ccd075642858b))

# [3.0.0](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.284...v3.0.0) (2024-10-23)


* chore!: can't have a default and non-default exports in commonjs ([ff5b428](https://github.com/scratchfoundation/scratch-storage/commit/ff5b428e7152b97671262da1d7ac08f3abfe7259))


### BREAKING CHANGES

* The ScratchStorage constructor is now exported
as a named export instead of a default export.

## [2.3.284](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.283...v2.3.284) (2024-10-23)


### Bug Fixes

* **deps:** lock file maintenance ([ceb347a](https://github.com/scratchfoundation/scratch-storage/commit/ceb347a1b98346d61d8407ef59e0cf68752fcf26))

## [2.3.283](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.282...v2.3.283) (2024-10-22)


### Bug Fixes

* **deps:** lock file maintenance ([162a3fb](https://github.com/scratchfoundation/scratch-storage/commit/162a3fb7404ca207e4e0b5991af3e87ada44314b))

## [2.3.282](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.281...v2.3.282) (2024-10-20)


### Bug Fixes

* **deps:** lock file maintenance ([c396683](https://github.com/scratchfoundation/scratch-storage/commit/c3966830ff320b13d2af49f23e1a530078daedf9))

## [2.3.281](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.280...v2.3.281) (2024-10-19)


### Bug Fixes

* **deps:** lock file maintenance ([479979f](https://github.com/scratchfoundation/scratch-storage/commit/479979fce84312cf82fb80e46aeb1ea1d6cc29c0))
* **deps:** lock file maintenance ([d36f71c](https://github.com/scratchfoundation/scratch-storage/commit/d36f71c6ad82e9b9b67de71d67512e8726eb884f))
* **deps:** lock file maintenance ([632d22a](https://github.com/scratchfoundation/scratch-storage/commit/632d22a0b60b6dcd40f36cba28ae10817b0f4725))
* **deps:** lock file maintenance ([a657cd8](https://github.com/scratchfoundation/scratch-storage/commit/a657cd8768021e49e008e202375a07beb4d90e58))

## [2.3.280](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.279...v2.3.280) (2024-10-15)


### Bug Fixes

* **deps:** lock file maintenance ([0baa15c](https://github.com/scratchfoundation/scratch-storage/commit/0baa15c44017f10225ead1595edf38c10f6ec4d8))

## [2.3.279](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.278...v2.3.279) (2024-10-15)


### Bug Fixes

* **deps:** lock file maintenance ([8447a35](https://github.com/scratchfoundation/scratch-storage/commit/8447a353aa6c36b33742cbe4be421849e829d8d8))

## [2.3.278](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.277...v2.3.278) (2024-10-12)


### Bug Fixes

* **deps:** lock file maintenance ([65cbf02](https://github.com/scratchfoundation/scratch-storage/commit/65cbf02cd1b70f3ca814d4edf20baca6f3a45b32))

## [2.3.277](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.276...v2.3.277) (2024-10-11)


### Bug Fixes

* **deps:** lock file maintenance ([fcdeed3](https://github.com/scratchfoundation/scratch-storage/commit/fcdeed3343b025006ae2681fd1730f760d11ca88))

## [2.3.276](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.275...v2.3.276) (2024-10-10)


### Bug Fixes

* **deps:** lock file maintenance ([fd51c9c](https://github.com/scratchfoundation/scratch-storage/commit/fd51c9c5acdd60d18ed02439058adacac7bc5b33))

## [2.3.275](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.274...v2.3.275) (2024-10-09)


### Bug Fixes

* **deps:** lock file maintenance ([1811513](https://github.com/scratchfoundation/scratch-storage/commit/18115138133761659242250782e5b64c1a65c4f9))

## [2.3.274](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.273...v2.3.274) (2024-10-08)


### Bug Fixes

* **deps:** lock file maintenance ([c12d658](https://github.com/scratchfoundation/scratch-storage/commit/c12d6589654f4a3f5ee2914e434067fa8413291e))

## [2.3.273](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.272...v2.3.273) (2024-10-06)


### Bug Fixes

* **deps:** lock file maintenance ([30bd9e7](https://github.com/scratchfoundation/scratch-storage/commit/30bd9e7013bf30574e4eba409982a5a2be205053))

## [2.3.272](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.271...v2.3.272) (2024-10-06)


### Bug Fixes

* **deps:** lock file maintenance ([7b0af34](https://github.com/scratchfoundation/scratch-storage/commit/7b0af343fc7356591f47ba0769a72eb7b527fe48))

## [2.3.271](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.270...v2.3.271) (2024-10-04)


### Bug Fixes

* **deps:** lock file maintenance ([c58f10e](https://github.com/scratchfoundation/scratch-storage/commit/c58f10e082a109b8eb1b0d1893a20653d7c19c28))

## [2.3.270](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.269...v2.3.270) (2024-10-04)


### Bug Fixes

* **deps:** lock file maintenance ([cde4646](https://github.com/scratchfoundation/scratch-storage/commit/cde4646b9ffab61147a9eb44e7dff3d51d7b2354))

## [2.3.269](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.268...v2.3.269) (2024-10-03)


### Bug Fixes

* **deps:** lock file maintenance ([07ac7e7](https://github.com/scratchfoundation/scratch-storage/commit/07ac7e7b99ba51381be5c6326b4acc0791f45888))

## [2.3.268](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.267...v2.3.268) (2024-10-02)


### Bug Fixes

* **deps:** lock file maintenance ([ddf8888](https://github.com/scratchfoundation/scratch-storage/commit/ddf88883de90f94a6847ae0459451f8b77b37266))

## [2.3.267](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.266...v2.3.267) (2024-10-02)


### Bug Fixes

* **deps:** lock file maintenance ([67a6915](https://github.com/scratchfoundation/scratch-storage/commit/67a6915726c3b8c442ab02f3c211406ed78c325c))

## [2.3.266](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.265...v2.3.266) (2024-10-01)


### Bug Fixes

* **deps:** lock file maintenance ([144b2ea](https://github.com/scratchfoundation/scratch-storage/commit/144b2ea8029a236160688fc489bf0882723df105))

## [2.3.265](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.264...v2.3.265) (2024-09-28)


### Bug Fixes

* **deps:** lock file maintenance ([c7c189d](https://github.com/scratchfoundation/scratch-storage/commit/c7c189d46a450b3eb33675be04c1aa8221c09c41))

## [2.3.264](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.263...v2.3.264) (2024-09-28)


### Bug Fixes

* **deps:** lock file maintenance ([e0dc279](https://github.com/scratchfoundation/scratch-storage/commit/e0dc2795c5bfce07a82c2d7bf3f583bca8cdb62b))

## [2.3.263](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.262...v2.3.263) (2024-09-27)


### Bug Fixes

* **deps:** lock file maintenance ([51ea0f8](https://github.com/scratchfoundation/scratch-storage/commit/51ea0f84f5d018b603fe39a9b3399e0b3b17ff23))

## [2.3.262](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.261...v2.3.262) (2024-09-26)


### Bug Fixes

* **deps:** lock file maintenance ([77b2e68](https://github.com/scratchfoundation/scratch-storage/commit/77b2e68572b7401cb184bf1f344d62441a07ab9f))

## [2.3.261](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.260...v2.3.261) (2024-09-25)


### Bug Fixes

* **deps:** lock file maintenance ([5d02197](https://github.com/scratchfoundation/scratch-storage/commit/5d02197b313d05b2701a0689358f798de86c910c))

## [2.3.260](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.259...v2.3.260) (2024-09-24)


### Bug Fixes

* **deps:** lock file maintenance ([80abe28](https://github.com/scratchfoundation/scratch-storage/commit/80abe286511ba65298beceab47266a89dcd466b2))

## [2.3.259](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.258...v2.3.259) (2024-09-24)


### Bug Fixes

* **deps:** lock file maintenance ([b9ce028](https://github.com/scratchfoundation/scratch-storage/commit/b9ce0280cd3828f5f04892d9a29c8dd955143bc4))

## [2.3.258](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.257...v2.3.258) (2024-09-23)


### Bug Fixes

* **deps:** lock file maintenance ([85b047e](https://github.com/scratchfoundation/scratch-storage/commit/85b047edd86eccdd6a3c67f4c7138bb79812483e))

## [2.3.257](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.256...v2.3.257) (2024-09-21)


### Bug Fixes

* **deps:** lock file maintenance ([80a5aeb](https://github.com/scratchfoundation/scratch-storage/commit/80a5aebf579edc185769bba3236d8436b0e7e178))

## [2.3.256](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.255...v2.3.256) (2024-09-21)


### Bug Fixes

* **deps:** lock file maintenance ([4b05798](https://github.com/scratchfoundation/scratch-storage/commit/4b05798b87ab32ac1b8d30ea30914ca3ddbacd7a))

## [2.3.255](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.254...v2.3.255) (2024-09-20)


### Bug Fixes

* **deps:** lock file maintenance ([938b93d](https://github.com/scratchfoundation/scratch-storage/commit/938b93d9cec90b8a43584e6f3ba2c4210eed71d5))

## [2.3.254](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.253...v2.3.254) (2024-09-19)


### Bug Fixes

* **deps:** lock file maintenance ([0c89f23](https://github.com/scratchfoundation/scratch-storage/commit/0c89f23f44965a131ece4ec9f051331a71e0e352))

## [2.3.253](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.252...v2.3.253) (2024-09-18)


### Bug Fixes

* **deps:** lock file maintenance ([804bd32](https://github.com/scratchfoundation/scratch-storage/commit/804bd32f14b91dca9092d83c91b53d3064af3b58))

## [2.3.252](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.251...v2.3.252) (2024-09-17)


### Bug Fixes

* **deps:** lock file maintenance ([ce2f6ec](https://github.com/scratchfoundation/scratch-storage/commit/ce2f6ecc9b11bb62b57122f29a1d63485f8f310f))

## [2.3.251](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.250...v2.3.251) (2024-09-16)


### Bug Fixes

* **deps:** lock file maintenance ([b7e3f53](https://github.com/scratchfoundation/scratch-storage/commit/b7e3f53bf6ea867f6ce8561303552cc2e52466f2))

## [2.3.250](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.249...v2.3.250) (2024-09-15)


### Bug Fixes

* **deps:** lock file maintenance ([40ed4a0](https://github.com/scratchfoundation/scratch-storage/commit/40ed4a076eb5132296bcd4a82ab3d2433705eb6d))

## [2.3.249](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.248...v2.3.249) (2024-09-14)


### Bug Fixes

* **deps:** lock file maintenance ([14998f2](https://github.com/scratchfoundation/scratch-storage/commit/14998f273fa40ce533b3b79856778d8d93cb97d7))

## [2.3.248](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.247...v2.3.248) (2024-09-13)


### Bug Fixes

* **deps:** lock file maintenance ([4e95479](https://github.com/scratchfoundation/scratch-storage/commit/4e95479683ec2b0b6ebb82090af480590eb5be32))

## [2.3.247](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.246...v2.3.247) (2024-09-13)


### Bug Fixes

* **deps:** lock file maintenance ([bf6c2df](https://github.com/scratchfoundation/scratch-storage/commit/bf6c2df898a0dfddc3067fcde6bfa895b99b6474))

## [2.3.246](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.245...v2.3.246) (2024-09-12)


### Bug Fixes

* **deps:** lock file maintenance ([97ed03e](https://github.com/scratchfoundation/scratch-storage/commit/97ed03eb5fb14d660cc4e3bb2d352ae9b05de20f))

## [2.3.245](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.244...v2.3.245) (2024-09-12)


### Bug Fixes

* **deps:** lock file maintenance ([427d9bd](https://github.com/scratchfoundation/scratch-storage/commit/427d9bd7a9b310cde85082e7ee78368b4e40e20f))

## [2.3.244](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.243...v2.3.244) (2024-09-11)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.16 ([6ce9b81](https://github.com/scratchfoundation/scratch-storage/commit/6ce9b81d0514cfcc77ef1bdf6bb4be302dc450f2))

## [2.3.243](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.242...v2.3.243) (2024-09-11)


### Bug Fixes

* **deps:** lock file maintenance ([a36bf67](https://github.com/scratchfoundation/scratch-storage/commit/a36bf676fb1eacc9028b5484994b9c93e3456622))

## [2.3.242](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.241...v2.3.242) (2024-09-11)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.15 ([47a03a2](https://github.com/scratchfoundation/scratch-storage/commit/47a03a29fc08c01aacb15742f72624bd9294bea2))

## [2.3.241](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.240...v2.3.241) (2024-09-10)


### Bug Fixes

* **deps:** lock file maintenance ([30b8e2b](https://github.com/scratchfoundation/scratch-storage/commit/30b8e2b43e2c3dd59f5b30637732cdd5d150efa2))

## [2.3.240](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.239...v2.3.240) (2024-09-10)


### Bug Fixes

* **deps:** lock file maintenance ([90f4296](https://github.com/scratchfoundation/scratch-storage/commit/90f4296ab67054c1f37bc5bd6ee78c3ddc379d65))

## [2.3.239](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.238...v2.3.239) (2024-09-09)


### Bug Fixes

* **deps:** lock file maintenance ([5597ba2](https://github.com/scratchfoundation/scratch-storage/commit/5597ba23c58acfbb6abe4f20f12c0218e01d48a7))

## [2.3.238](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.237...v2.3.238) (2024-09-08)


### Bug Fixes

* **deps:** lock file maintenance ([428f20a](https://github.com/scratchfoundation/scratch-storage/commit/428f20af80811c0436c43c30d665fe7de8dd6efe))

## [2.3.237](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.236...v2.3.237) (2024-09-07)


### Bug Fixes

* **deps:** lock file maintenance ([ab402c0](https://github.com/scratchfoundation/scratch-storage/commit/ab402c0eccd04c5313fe66e5ef1f33c1adb9b03c))

## [2.3.236](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.235...v2.3.236) (2024-09-06)


### Bug Fixes

* **deps:** lock file maintenance ([1adfa1e](https://github.com/scratchfoundation/scratch-storage/commit/1adfa1e20fd5c5b4d70cea0d52be3a0de7efe32c))

## [2.3.235](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.234...v2.3.235) (2024-09-05)


### Bug Fixes

* **deps:** lock file maintenance ([66f7d47](https://github.com/scratchfoundation/scratch-storage/commit/66f7d4733cb814a34d3288aa1baa0afe510daaac))

## [2.3.234](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.233...v2.3.234) (2024-09-04)


### Bug Fixes

* **deps:** lock file maintenance ([622723c](https://github.com/scratchfoundation/scratch-storage/commit/622723c88853da217f37798dcc6af51c7dcd3308))

## [2.3.233](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.232...v2.3.233) (2024-09-03)


### Bug Fixes

* **deps:** lock file maintenance ([1169792](https://github.com/scratchfoundation/scratch-storage/commit/1169792ad8add788951510d539ec02754d23051c))

## [2.3.232](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.231...v2.3.232) (2024-09-02)


### Bug Fixes

* **deps:** lock file maintenance ([79cd9e5](https://github.com/scratchfoundation/scratch-storage/commit/79cd9e5c2959c51e45bfadd816994b92ebe49817))

## [2.3.231](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.230...v2.3.231) (2024-08-31)


### Bug Fixes

* **deps:** lock file maintenance ([1a3ea81](https://github.com/scratchfoundation/scratch-storage/commit/1a3ea815b056fdc10f8c76c762053ac567adbc3a))

## [2.3.230](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.229...v2.3.230) (2024-08-30)


### Bug Fixes

* **deps:** lock file maintenance ([920a963](https://github.com/scratchfoundation/scratch-storage/commit/920a963644091f2772cc9a6dbd90942fb93b981a))

## [2.3.229](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.228...v2.3.229) (2024-08-28)


### Bug Fixes

* **deps:** lock file maintenance ([7f918ed](https://github.com/scratchfoundation/scratch-storage/commit/7f918edd35664d76a8366921a2da798307f5a01e))

## [2.3.228](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.227...v2.3.228) (2024-08-26)


### Bug Fixes

* **deps:** lock file maintenance ([9af9c01](https://github.com/scratchfoundation/scratch-storage/commit/9af9c012294d496b1cca197b8a41b2e6f5577041))

## [2.3.227](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.226...v2.3.227) (2024-08-25)


### Bug Fixes

* **deps:** lock file maintenance ([f58faf6](https://github.com/scratchfoundation/scratch-storage/commit/f58faf6ace0cc14cbed24fbd268c512e48905084))

## [2.3.226](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.225...v2.3.226) (2024-08-24)


### Bug Fixes

* **deps:** lock file maintenance ([83b9eb5](https://github.com/scratchfoundation/scratch-storage/commit/83b9eb512c8f0cddd58ef92432f020eba888fb01))

## [2.3.225](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.224...v2.3.225) (2024-08-24)


### Bug Fixes

* **deps:** lock file maintenance ([5de3fb9](https://github.com/scratchfoundation/scratch-storage/commit/5de3fb972061385ed76884382226a303c8fb6fca))

## [2.3.224](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.223...v2.3.224) (2024-08-23)


### Bug Fixes

* **deps:** lock file maintenance ([c7eadc7](https://github.com/scratchfoundation/scratch-storage/commit/c7eadc755a852aa95fe6be7924eb7662873ab0b0))

## [2.3.223](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.222...v2.3.223) (2024-08-22)


### Bug Fixes

* **deps:** lock file maintenance ([1f6aede](https://github.com/scratchfoundation/scratch-storage/commit/1f6aede0912eb1b0ab33b99cf7d40c75858a42ba))

## [2.3.222](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.221...v2.3.222) (2024-08-22)


### Bug Fixes

* **deps:** lock file maintenance ([5173fa7](https://github.com/scratchfoundation/scratch-storage/commit/5173fa7af7c84d144aab53ce3ba3033fdc15a40a))

## [2.3.221](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.220...v2.3.221) (2024-08-21)


### Bug Fixes

* **deps:** lock file maintenance ([b061043](https://github.com/scratchfoundation/scratch-storage/commit/b0610430607cb26d0017c17a8e64617e9601661b))

## [2.3.220](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.219...v2.3.220) (2024-08-21)


### Bug Fixes

* **deps:** lock file maintenance ([b1df733](https://github.com/scratchfoundation/scratch-storage/commit/b1df73304bf10a25e2070cf5136e44eaec98e6e9))

## [2.3.219](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.218...v2.3.219) (2024-08-20)


### Bug Fixes

* **deps:** lock file maintenance ([a5eee99](https://github.com/scratchfoundation/scratch-storage/commit/a5eee994e0680da29ea3baf8a5143e82b625745b))

## [2.3.218](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.217...v2.3.218) (2024-08-19)


### Bug Fixes

* **deps:** lock file maintenance ([a4d0906](https://github.com/scratchfoundation/scratch-storage/commit/a4d09060117705006d1532e941002f46cb381625))

## [2.3.217](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.216...v2.3.217) (2024-08-18)


### Bug Fixes

* **deps:** lock file maintenance ([94363e0](https://github.com/scratchfoundation/scratch-storage/commit/94363e0e9033c070bb370c0094aa234654059bfe))

## [2.3.216](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.215...v2.3.216) (2024-08-18)


### Bug Fixes

* **deps:** lock file maintenance ([24e5aa7](https://github.com/scratchfoundation/scratch-storage/commit/24e5aa7d4dbb0fdfeead32236c77e410f1c6a04a))

## [2.3.215](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.214...v2.3.215) (2024-08-17)


### Bug Fixes

* **deps:** lock file maintenance ([939f2d7](https://github.com/scratchfoundation/scratch-storage/commit/939f2d7bb6e09c949a7ad05981a214ba1add3ad7))

## [2.3.214](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.213...v2.3.214) (2024-08-16)


### Bug Fixes

* **deps:** lock file maintenance ([1713058](https://github.com/scratchfoundation/scratch-storage/commit/1713058f36ece4c95aa1b9255ac6b6cf9f895b53))

## [2.3.213](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.212...v2.3.213) (2024-08-15)


### Bug Fixes

* **deps:** lock file maintenance ([ae0973a](https://github.com/scratchfoundation/scratch-storage/commit/ae0973a51d8b637cb9850782f2f4bbec57bd0bdc))

## [2.3.212](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.211...v2.3.212) (2024-08-15)


### Bug Fixes

* **deps:** lock file maintenance ([8d0c6b6](https://github.com/scratchfoundation/scratch-storage/commit/8d0c6b6d6dbd1482a21f10d1336a8cc700791977))

## [2.3.211](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.210...v2.3.211) (2024-08-14)


### Bug Fixes

* **deps:** lock file maintenance ([36d46c4](https://github.com/scratchfoundation/scratch-storage/commit/36d46c4bdfd0f313946f6d5d7dad7ae6a023656b))

## [2.3.210](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.209...v2.3.210) (2024-08-13)


### Bug Fixes

* **deps:** lock file maintenance ([292c48d](https://github.com/scratchfoundation/scratch-storage/commit/292c48d96c6d28f799758ee725f379f0739c598a))

## [2.3.209](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.208...v2.3.209) (2024-08-12)


### Bug Fixes

* **deps:** lock file maintenance ([e70d3d9](https://github.com/scratchfoundation/scratch-storage/commit/e70d3d97e92c8476c2a7acb057b4c0729dc8dffa))

## [2.3.208](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.207...v2.3.208) (2024-08-11)


### Bug Fixes

* **deps:** lock file maintenance ([035778f](https://github.com/scratchfoundation/scratch-storage/commit/035778f8603861d074be3420eebf2e23e401d06b))

## [2.3.207](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.206...v2.3.207) (2024-08-10)


### Bug Fixes

* **deps:** lock file maintenance ([a8903be](https://github.com/scratchfoundation/scratch-storage/commit/a8903beb61d1e8267635d28144bbd682aa067b7a))

## [2.3.206](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.205...v2.3.206) (2024-08-09)


### Bug Fixes

* **deps:** lock file maintenance ([999c032](https://github.com/scratchfoundation/scratch-storage/commit/999c032f72bde30492d89e7811eb06ebfb608e12))

## [2.3.205](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.204...v2.3.205) (2024-08-08)


### Bug Fixes

* **deps:** lock file maintenance ([d8c213a](https://github.com/scratchfoundation/scratch-storage/commit/d8c213aff64f3bb8da2ee74ab0f462c6e880b576))

## [2.3.204](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.203...v2.3.204) (2024-08-07)


### Bug Fixes

* **deps:** lock file maintenance ([628eefe](https://github.com/scratchfoundation/scratch-storage/commit/628eefe4b2870950120560f6d05baedeaabdc88e))

## [2.3.203](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.202...v2.3.203) (2024-08-05)


### Bug Fixes

* **deps:** lock file maintenance ([d06302e](https://github.com/scratchfoundation/scratch-storage/commit/d06302e6e3497fa8755637721d0c6d9c92ea7e57))

## [2.3.202](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.201...v2.3.202) (2024-08-05)


### Bug Fixes

* **deps:** lock file maintenance ([69d2bf2](https://github.com/scratchfoundation/scratch-storage/commit/69d2bf2bd1537d9b30b2709efa3711338534d6e8))

## [2.3.201](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.200...v2.3.201) (2024-08-03)


### Bug Fixes

* **deps:** lock file maintenance ([bc3152d](https://github.com/scratchfoundation/scratch-storage/commit/bc3152de76b104eb8ef2119db6b707e59268a050))

## [2.3.200](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.199...v2.3.200) (2024-08-03)


### Bug Fixes

* **deps:** lock file maintenance ([ef46c68](https://github.com/scratchfoundation/scratch-storage/commit/ef46c680fe4b5451bad29eeeed7af20924e1dfb9))

## [2.3.199](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.198...v2.3.199) (2024-07-31)


### Bug Fixes

* **deps:** lock file maintenance ([9a001ab](https://github.com/scratchfoundation/scratch-storage/commit/9a001abef9bd6f4c41b1caf9844523cb3a654f7a))

## [2.3.198](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.197...v2.3.198) (2024-07-29)


### Bug Fixes

* **deps:** lock file maintenance ([143170a](https://github.com/scratchfoundation/scratch-storage/commit/143170af6ceea509beeaf22d24a20358360ec027))

## [2.3.197](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.196...v2.3.197) (2024-07-28)


### Bug Fixes

* **deps:** lock file maintenance ([734f080](https://github.com/scratchfoundation/scratch-storage/commit/734f080568b51868d63d21965f0dd7b9c3360f99))

## [2.3.196](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.195...v2.3.196) (2024-07-28)


### Bug Fixes

* **deps:** lock file maintenance ([9ecedf1](https://github.com/scratchfoundation/scratch-storage/commit/9ecedf1645a1ff6f5245ea1936910107d996503b))

## [2.3.195](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.194...v2.3.195) (2024-07-27)


### Bug Fixes

* **deps:** lock file maintenance ([712f258](https://github.com/scratchfoundation/scratch-storage/commit/712f2581d05ffc128153a0f1bb729810875043b2))

## [2.3.194](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.193...v2.3.194) (2024-07-26)


### Bug Fixes

* **deps:** lock file maintenance ([a26c05f](https://github.com/scratchfoundation/scratch-storage/commit/a26c05f7a35c993b5500bf2b8fd1397c8ef3542f))

## [2.3.193](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.192...v2.3.193) (2024-07-25)


### Bug Fixes

* **deps:** lock file maintenance ([9973590](https://github.com/scratchfoundation/scratch-storage/commit/9973590074222d2930b221a511ea2bb1da2e839f))

## [2.3.192](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.191...v2.3.192) (2024-07-24)


### Bug Fixes

* **deps:** lock file maintenance ([f186f52](https://github.com/scratchfoundation/scratch-storage/commit/f186f525069897104cfd7ddcba4816e73cba82e3))

## [2.3.191](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.190...v2.3.191) (2024-07-24)


### Bug Fixes

* **deps:** lock file maintenance ([114c1c2](https://github.com/scratchfoundation/scratch-storage/commit/114c1c21a5e6d6b4e5839335dcda1d26ed6509d0))

## [2.3.190](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.189...v2.3.190) (2024-07-23)


### Bug Fixes

* **deps:** lock file maintenance ([464431d](https://github.com/scratchfoundation/scratch-storage/commit/464431da87f317753d760eec852ae57b8b4c0408))

## [2.3.189](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.188...v2.3.189) (2024-07-22)


### Bug Fixes

* **deps:** lock file maintenance ([96a5305](https://github.com/scratchfoundation/scratch-storage/commit/96a530501b1b7ae20225fbbfffa09f2047c4902e))

## [2.3.188](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.187...v2.3.188) (2024-07-21)


### Bug Fixes

* **deps:** lock file maintenance ([cf4789d](https://github.com/scratchfoundation/scratch-storage/commit/cf4789d7a88f4c67247468b5a1238a09ea800846))

## [2.3.187](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.186...v2.3.187) (2024-07-20)


### Bug Fixes

* **deps:** lock file maintenance ([62c0caa](https://github.com/scratchfoundation/scratch-storage/commit/62c0caab6c1798e6fe5d211b6ca39aa4b57d529d))

## [2.3.186](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.185...v2.3.186) (2024-07-19)


### Bug Fixes

* **deps:** lock file maintenance ([807395d](https://github.com/scratchfoundation/scratch-storage/commit/807395d3146b503652d8435c491cc6d3e4ce4e1d))

## [2.3.185](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.184...v2.3.185) (2024-07-18)


### Bug Fixes

* **deps:** lock file maintenance ([c390161](https://github.com/scratchfoundation/scratch-storage/commit/c390161f62082df0bd116e093d99781fce9455c0))

## [2.3.184](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.183...v2.3.184) (2024-07-17)


### Bug Fixes

* **deps:** lock file maintenance ([511cafe](https://github.com/scratchfoundation/scratch-storage/commit/511cafede1eb9bdb2ad23c5b3dd052ddc6139753))

## [2.3.183](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.182...v2.3.183) (2024-07-16)


### Bug Fixes

* **deps:** lock file maintenance ([ee94103](https://github.com/scratchfoundation/scratch-storage/commit/ee94103b42e273d896c832f0257d3cf983c3d3d5))

## [2.3.182](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.181...v2.3.182) (2024-07-16)


### Bug Fixes

* **deps:** lock file maintenance ([8e7afa2](https://github.com/scratchfoundation/scratch-storage/commit/8e7afa2d170a7b87704d7dd93fc2ae3f3522b05a))

## [2.3.181](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.180...v2.3.181) (2024-07-14)


### Bug Fixes

* **deps:** lock file maintenance ([90c4e8b](https://github.com/scratchfoundation/scratch-storage/commit/90c4e8b0a3e343c76b3faf185b0edaa34ac6381d))

## [2.3.180](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.179...v2.3.180) (2024-07-13)


### Bug Fixes

* **deps:** lock file maintenance ([3803d24](https://github.com/scratchfoundation/scratch-storage/commit/3803d24291da1fe3bb4ddf5e4143e43f18ca6db4))

## [2.3.179](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.178...v2.3.179) (2024-07-12)


### Bug Fixes

* **deps:** lock file maintenance ([0431f29](https://github.com/scratchfoundation/scratch-storage/commit/0431f2959cea5a05c26924fe2b6c86dcb893f28c))

## [2.3.178](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.177...v2.3.178) (2024-07-12)


### Bug Fixes

* **deps:** lock file maintenance ([ff9a9d6](https://github.com/scratchfoundation/scratch-storage/commit/ff9a9d63f21bcc0be197b2636e66fc95d77400b9))

## [2.3.177](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.176...v2.3.177) (2024-07-11)


### Bug Fixes

* **deps:** lock file maintenance ([8eac843](https://github.com/scratchfoundation/scratch-storage/commit/8eac843d0db8e68ff562b984eeb16feb3d5908cd))

## [2.3.176](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.175...v2.3.176) (2024-07-10)


### Bug Fixes

* **deps:** lock file maintenance ([7769522](https://github.com/scratchfoundation/scratch-storage/commit/77695224fe2acb6b3c0b632a5440526f24ca1d31))

## [2.3.175](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.174...v2.3.175) (2024-07-09)


### Bug Fixes

* **deps:** lock file maintenance ([c5c68cd](https://github.com/scratchfoundation/scratch-storage/commit/c5c68cd6ad3b362c61aef9d385e44235dd982cf8))

## [2.3.174](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.173...v2.3.174) (2024-07-09)


### Bug Fixes

* **deps:** lock file maintenance ([039f5f4](https://github.com/scratchfoundation/scratch-storage/commit/039f5f4147942e720906b0dca1cf1143efc9c787))

## [2.3.173](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.172...v2.3.173) (2024-07-07)


### Bug Fixes

* **deps:** lock file maintenance ([05a1544](https://github.com/scratchfoundation/scratch-storage/commit/05a15440724770d409ed9307b7d49455f7d03e89))

## [2.3.172](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.171...v2.3.172) (2024-07-06)


### Bug Fixes

* **deps:** lock file maintenance ([34f5cc9](https://github.com/scratchfoundation/scratch-storage/commit/34f5cc9f2e84d89fe23be871725dbe177103db8c))

## [2.3.171](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.170...v2.3.171) (2024-07-03)


### Bug Fixes

* **deps:** lock file maintenance ([d42f66d](https://github.com/scratchfoundation/scratch-storage/commit/d42f66dc3712b1e989e13f252b7fdb4c145744ea))

## [2.3.170](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.169...v2.3.170) (2024-07-03)


### Bug Fixes

* **deps:** lock file maintenance ([287218d](https://github.com/scratchfoundation/scratch-storage/commit/287218db5550524799a48255a0a249822f7b26f3))

## [2.3.169](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.168...v2.3.169) (2024-07-02)


### Bug Fixes

* **deps:** lock file maintenance ([407b6ee](https://github.com/scratchfoundation/scratch-storage/commit/407b6eeb21d1b13e1072be53d3db8588d560dffb))

## [2.3.168](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.167...v2.3.168) (2024-07-02)


### Bug Fixes

* **deps:** lock file maintenance ([9261c0e](https://github.com/scratchfoundation/scratch-storage/commit/9261c0ec79e0577fe64c2d79db97ca3a57f4e201))

## [2.3.167](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.166...v2.3.167) (2024-07-01)


### Bug Fixes

* **deps:** lock file maintenance ([7af7ce6](https://github.com/scratchfoundation/scratch-storage/commit/7af7ce6af6eee1dd2e6291e66a426230f54b12cc))

## [2.3.166](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.165...v2.3.166) (2024-06-30)


### Bug Fixes

* **deps:** lock file maintenance ([e09d709](https://github.com/scratchfoundation/scratch-storage/commit/e09d7096f0fbea39fee777ec01aed5b5f9519c71))

## [2.3.165](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.164...v2.3.165) (2024-06-29)


### Bug Fixes

* **deps:** lock file maintenance ([e3239c4](https://github.com/scratchfoundation/scratch-storage/commit/e3239c40413bd089474b15e835117fad427fa954))

## [2.3.164](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.163...v2.3.164) (2024-06-28)


### Bug Fixes

* **deps:** lock file maintenance ([d97d2b0](https://github.com/scratchfoundation/scratch-storage/commit/d97d2b0be1b88a815ef88ac4e2d07d56a82cb1a3))

## [2.3.163](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.162...v2.3.163) (2024-06-27)


### Bug Fixes

* **deps:** lock file maintenance ([0a4516e](https://github.com/scratchfoundation/scratch-storage/commit/0a4516e6db76cdbce97e3aa567bcc8d8c6bb96d0))

## [2.3.162](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.161...v2.3.162) (2024-06-26)


### Bug Fixes

* **deps:** lock file maintenance ([b74eec7](https://github.com/scratchfoundation/scratch-storage/commit/b74eec7e5d254fd21b54ad56dc57bae26b050473))

## [2.3.161](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.160...v2.3.161) (2024-06-26)


### Bug Fixes

* **deps:** lock file maintenance ([a87c526](https://github.com/scratchfoundation/scratch-storage/commit/a87c5264838c70c92681133d5a497594a4ab5119))

## [2.3.160](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.159...v2.3.160) (2024-06-25)


### Bug Fixes

* **deps:** lock file maintenance ([64106a2](https://github.com/scratchfoundation/scratch-storage/commit/64106a2057712cd64e9e1b409d02dd1dd1eecb3a))

## [2.3.159](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.158...v2.3.159) (2024-06-23)


### Bug Fixes

* **deps:** lock file maintenance ([d1b3b2f](https://github.com/scratchfoundation/scratch-storage/commit/d1b3b2feeabdfe9799921a6c8723f54441bca22b))

## [2.3.158](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.157...v2.3.158) (2024-06-22)


### Bug Fixes

* **deps:** lock file maintenance ([ed4edf8](https://github.com/scratchfoundation/scratch-storage/commit/ed4edf826bf753c732cc85724f32b171315e69ab))

## [2.3.157](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.156...v2.3.157) (2024-06-22)


### Bug Fixes

* **deps:** lock file maintenance ([7bb4997](https://github.com/scratchfoundation/scratch-storage/commit/7bb4997f5440d6e4538c8b6a87c857fbc70054e8))

## [2.3.156](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.155...v2.3.156) (2024-06-22)


### Bug Fixes

* **deps:** lock file maintenance ([82a0e1b](https://github.com/scratchfoundation/scratch-storage/commit/82a0e1b7d8d1546d2b0f672dd1af860716594e97))

## [2.3.155](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.154...v2.3.155) (2024-06-21)


### Bug Fixes

* **deps:** lock file maintenance ([54570de](https://github.com/scratchfoundation/scratch-storage/commit/54570de38692c47eec25e23c87bc1aa26f8821dc))

## [2.3.154](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.153...v2.3.154) (2024-06-20)


### Bug Fixes

* **deps:** lock file maintenance ([e22bf6d](https://github.com/scratchfoundation/scratch-storage/commit/e22bf6d05ae32ca298df272873328f3d42c1e1f0))

## [2.3.153](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.152...v2.3.153) (2024-06-19)


### Bug Fixes

* **deps:** lock file maintenance ([4cb3a57](https://github.com/scratchfoundation/scratch-storage/commit/4cb3a570d52602c7c354d0511752a4a420f3c249))

## [2.3.152](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.151...v2.3.152) (2024-06-18)


### Bug Fixes

* **deps:** lock file maintenance ([914df8b](https://github.com/scratchfoundation/scratch-storage/commit/914df8bbcba1e0a36b8371985bba35c504be17cf))

## [2.3.151](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.150...v2.3.151) (2024-06-17)


### Bug Fixes

* **deps:** lock file maintenance ([387faaa](https://github.com/scratchfoundation/scratch-storage/commit/387faaa07ce06b94b5096ef9790b294d395e4001))

## [2.3.150](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.149...v2.3.150) (2024-06-15)


### Bug Fixes

* **deps:** lock file maintenance ([8c9895f](https://github.com/scratchfoundation/scratch-storage/commit/8c9895fb216cc82a6d82df0f1678b2e353c6ef86))

## [2.3.149](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.148...v2.3.149) (2024-06-14)


### Bug Fixes

* **deps:** lock file maintenance ([c60aaa8](https://github.com/scratchfoundation/scratch-storage/commit/c60aaa8c4d20124e0b458e39377f71ff257b5b20))

## [2.3.148](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.147...v2.3.148) (2024-06-14)


### Bug Fixes

* **deps:** lock file maintenance ([8e1a24f](https://github.com/scratchfoundation/scratch-storage/commit/8e1a24f92881517146c031e19ad68305faeb7968))

## [2.3.147](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.146...v2.3.147) (2024-06-13)


### Bug Fixes

* **deps:** lock file maintenance ([7359f45](https://github.com/scratchfoundation/scratch-storage/commit/7359f457f5bcdfcb700aa5918494205944ef4053))

## [2.3.146](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.145...v2.3.146) (2024-06-13)


### Bug Fixes

* **deps:** lock file maintenance ([ab5cc93](https://github.com/scratchfoundation/scratch-storage/commit/ab5cc93a9449948fb719bb17a069684c8b6e7023))

## [2.3.145](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.144...v2.3.145) (2024-06-12)


### Bug Fixes

* **deps:** lock file maintenance ([2701061](https://github.com/scratchfoundation/scratch-storage/commit/2701061835618b9aa02cc69f6ed955185f3f1a1f))

## [2.3.144](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.143...v2.3.144) (2024-06-11)


### Bug Fixes

* **deps:** lock file maintenance ([92a34fb](https://github.com/scratchfoundation/scratch-storage/commit/92a34fb57e0a83140aa981c89be9cd8abfb1f95f))

## [2.3.143](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.142...v2.3.143) (2024-06-10)


### Bug Fixes

* **deps:** lock file maintenance ([8a009df](https://github.com/scratchfoundation/scratch-storage/commit/8a009df8f8bce2226f2a701edc008b2fd6866214))

## [2.3.142](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.141...v2.3.142) (2024-06-10)


### Bug Fixes

* **deps:** lock file maintenance ([8a79c9e](https://github.com/scratchfoundation/scratch-storage/commit/8a79c9e5db9404d3bbf24f023027593a089aaf85))

## [2.3.141](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.140...v2.3.141) (2024-06-09)


### Bug Fixes

* **deps:** lock file maintenance ([ee79b49](https://github.com/scratchfoundation/scratch-storage/commit/ee79b4982880a8a2f20bb9ecb79b27874561e85a))

## [2.3.140](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.139...v2.3.140) (2024-06-08)


### Bug Fixes

* **deps:** lock file maintenance ([a24718e](https://github.com/scratchfoundation/scratch-storage/commit/a24718ee5feb0ddf8f4cff2c5b6875786aa38f81))

## [2.3.139](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.138...v2.3.139) (2024-06-07)


### Bug Fixes

* **deps:** lock file maintenance ([d5c5d15](https://github.com/scratchfoundation/scratch-storage/commit/d5c5d157f6f9fbd52e4c9520bd98c1405b0bb117))

## [2.3.138](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.137...v2.3.138) (2024-06-07)


### Bug Fixes

* **deps:** lock file maintenance ([d4c6ad3](https://github.com/scratchfoundation/scratch-storage/commit/d4c6ad375744670e08e985a72eb292025efc6286))

## [2.3.137](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.136...v2.3.137) (2024-06-06)


### Bug Fixes

* **deps:** lock file maintenance ([261e942](https://github.com/scratchfoundation/scratch-storage/commit/261e9426d6f560b9b740c5b29061e222499dfa6d))

## [2.3.136](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.135...v2.3.136) (2024-06-05)


### Bug Fixes

* **deps:** lock file maintenance ([61b007e](https://github.com/scratchfoundation/scratch-storage/commit/61b007e492be86b6aab8ad0398eda968331ccc81))

## [2.3.135](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.134...v2.3.135) (2024-06-04)


### Bug Fixes

* **deps:** lock file maintenance ([b38dcee](https://github.com/scratchfoundation/scratch-storage/commit/b38dcee8bc86fd6f2838a815b6e48351c4ef936b))

## [2.3.134](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.133...v2.3.134) (2024-06-03)


### Bug Fixes

* **deps:** lock file maintenance ([baa587b](https://github.com/scratchfoundation/scratch-storage/commit/baa587b25364281d4e2e3fdc4131e31ca04b0e08))

## [2.3.133](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.132...v2.3.133) (2024-06-03)


### Bug Fixes

* **deps:** lock file maintenance ([d3a11e3](https://github.com/scratchfoundation/scratch-storage/commit/d3a11e305e29e2ed0d3010b6e6f5ea9b6c38d168))

## [2.3.132](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.131...v2.3.132) (2024-06-02)


### Bug Fixes

* **deps:** lock file maintenance ([7ab052b](https://github.com/scratchfoundation/scratch-storage/commit/7ab052b87137082830b6845f3a5c3840afbd9a72))

## [2.3.131](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.130...v2.3.131) (2024-06-01)


### Bug Fixes

* **deps:** lock file maintenance ([782c107](https://github.com/scratchfoundation/scratch-storage/commit/782c107e19bf892d9a502288860c731a04269ddb))

## [2.3.130](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.129...v2.3.130) (2024-05-31)


### Bug Fixes

* **deps:** lock file maintenance ([c213f0b](https://github.com/scratchfoundation/scratch-storage/commit/c213f0be534a83f44121a2b2c7260b92e1f7579d))

## [2.3.129](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.128...v2.3.129) (2024-05-31)


### Bug Fixes

* **deps:** lock file maintenance ([8be10dc](https://github.com/scratchfoundation/scratch-storage/commit/8be10dc8af5ef1de5b124c8ebe179aaee33bac66))

## [2.3.128](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.127...v2.3.128) (2024-05-30)


### Bug Fixes

* **deps:** lock file maintenance ([9a33c09](https://github.com/scratchfoundation/scratch-storage/commit/9a33c09554c5f195a87c971c706d3d002532c5fb))

## [2.3.127](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.126...v2.3.127) (2024-05-28)


### Bug Fixes

* **deps:** lock file maintenance ([1e3f4eb](https://github.com/scratchfoundation/scratch-storage/commit/1e3f4eb2ec0061406c0d3ce0954ebca4d2375904))

## [2.3.126](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.125...v2.3.126) (2024-05-27)


### Bug Fixes

* **deps:** lock file maintenance ([80e7e0a](https://github.com/scratchfoundation/scratch-storage/commit/80e7e0a323ee2175cfc474de6d54dcc848232f1b))

## [2.3.125](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.124...v2.3.125) (2024-05-26)


### Bug Fixes

* **deps:** lock file maintenance ([059f062](https://github.com/scratchfoundation/scratch-storage/commit/059f062debc663d47ab141385afab44f08bb44c2))

## [2.3.124](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.123...v2.3.124) (2024-05-25)


### Bug Fixes

* **deps:** lock file maintenance ([5d0b7a8](https://github.com/scratchfoundation/scratch-storage/commit/5d0b7a84e723a0e65e2205236583c5e66ef0b549))

## [2.3.123](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.122...v2.3.123) (2024-05-25)


### Bug Fixes

* **deps:** lock file maintenance ([27364df](https://github.com/scratchfoundation/scratch-storage/commit/27364dfe2e12578729cfa89c32369b1e4106d42b))

## [2.3.122](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.121...v2.3.122) (2024-05-24)


### Bug Fixes

* **deps:** lock file maintenance ([3feafbc](https://github.com/scratchfoundation/scratch-storage/commit/3feafbc22284bef92fa02d1eb87abb90227f1e7d))

## [2.3.121](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.120...v2.3.121) (2024-05-24)


### Bug Fixes

* **deps:** lock file maintenance ([94981a1](https://github.com/scratchfoundation/scratch-storage/commit/94981a199cf8f5bd43d6056a8100c9c0653f669f))

## [2.3.120](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.119...v2.3.120) (2024-05-23)


### Bug Fixes

* **deps:** lock file maintenance ([635fc40](https://github.com/scratchfoundation/scratch-storage/commit/635fc4077d3e5f225d2de42fa7ab9f541d464149))

## [2.3.119](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.118...v2.3.119) (2024-05-23)


### Bug Fixes

* **deps:** lock file maintenance ([c30f5b5](https://github.com/scratchfoundation/scratch-storage/commit/c30f5b5f5c96d897e4d024317815f0f63e617cd6))

## [2.3.118](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.117...v2.3.118) (2024-05-21)


### Bug Fixes

* **deps:** lock file maintenance ([47ed1c1](https://github.com/scratchfoundation/scratch-storage/commit/47ed1c11b757c0f76eaa330b51b51ca9ad34deb1))

## [2.3.117](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.116...v2.3.117) (2024-05-19)


### Bug Fixes

* **deps:** lock file maintenance ([3c51096](https://github.com/scratchfoundation/scratch-storage/commit/3c51096369f03bc1c935c301f4961d5642d08505))

## [2.3.116](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.115...v2.3.116) (2024-05-18)


### Bug Fixes

* **deps:** lock file maintenance ([813c676](https://github.com/scratchfoundation/scratch-storage/commit/813c6762bfc6c189cde006608ed60fa9b3833f28))

## [2.3.115](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.114...v2.3.115) (2024-05-17)


### Bug Fixes

* **deps:** lock file maintenance ([da9ef1c](https://github.com/scratchfoundation/scratch-storage/commit/da9ef1c948a5741453a6d3167d1d54742bc955d9))

## [2.3.114](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.113...v2.3.114) (2024-05-15)


### Bug Fixes

* **deps:** lock file maintenance ([a3dc68c](https://github.com/scratchfoundation/scratch-storage/commit/a3dc68cb1fc31577ff344540de6783f562dd608b))

## [2.3.113](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.112...v2.3.113) (2024-05-14)


### Bug Fixes

* **deps:** lock file maintenance ([29a86f9](https://github.com/scratchfoundation/scratch-storage/commit/29a86f978ec287fc5bd6aa5f5bab9907b868929d))

## [2.3.112](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.111...v2.3.112) (2024-05-14)


### Bug Fixes

* **deps:** lock file maintenance ([dc3c0f0](https://github.com/scratchfoundation/scratch-storage/commit/dc3c0f0e10715055821d9d13d0a9bcfd3076549c))

## [2.3.111](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.110...v2.3.111) (2024-05-13)


### Bug Fixes

* **deps:** lock file maintenance ([9c87318](https://github.com/scratchfoundation/scratch-storage/commit/9c873181488997244c1002613e3fc1d6cf400874))

## [2.3.110](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.109...v2.3.110) (2024-05-11)


### Bug Fixes

* **deps:** lock file maintenance ([da0b6c0](https://github.com/scratchfoundation/scratch-storage/commit/da0b6c0bdb45e2f2608e193f7005bd9d01a3c1c4))

## [2.3.109](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.108...v2.3.109) (2024-05-11)


### Bug Fixes

* **deps:** lock file maintenance ([90b17e0](https://github.com/scratchfoundation/scratch-storage/commit/90b17e04272216e45154e42f19572f1383bf2598))

## [2.3.108](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.107...v2.3.108) (2024-05-10)


### Bug Fixes

* **deps:** lock file maintenance ([00c43f9](https://github.com/scratchfoundation/scratch-storage/commit/00c43f99dd34b8cee2d1db7116125936667f1f2a))

## [2.3.107](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.106...v2.3.107) (2024-05-09)


### Bug Fixes

* **deps:** lock file maintenance ([43a2a7b](https://github.com/scratchfoundation/scratch-storage/commit/43a2a7b6914d48a1aab0ed1854d8976a41f2f42d))

## [2.3.106](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.105...v2.3.106) (2024-05-08)


### Bug Fixes

* **deps:** lock file maintenance ([3f33be3](https://github.com/scratchfoundation/scratch-storage/commit/3f33be3216d7015a8f708ffe271510ad401b2562))

## [2.3.105](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.104...v2.3.105) (2024-05-07)


### Bug Fixes

* **deps:** lock file maintenance ([a9af681](https://github.com/scratchfoundation/scratch-storage/commit/a9af68127d9f0386f538a0e4d03bb7e4d0d11623))

## [2.3.104](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.103...v2.3.104) (2024-05-06)


### Bug Fixes

* **deps:** lock file maintenance ([4162f25](https://github.com/scratchfoundation/scratch-storage/commit/4162f2507e2fe7c873abf0c5629c735ddc1675af))

## [2.3.103](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.102...v2.3.103) (2024-05-05)


### Bug Fixes

* **deps:** lock file maintenance ([568cb67](https://github.com/scratchfoundation/scratch-storage/commit/568cb67d1f0f1cdf4881d9b086bfb294ea874d6d))

## [2.3.102](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.101...v2.3.102) (2024-05-04)


### Bug Fixes

* **deps:** lock file maintenance ([82e81e4](https://github.com/scratchfoundation/scratch-storage/commit/82e81e4670890dbc6b9fecf3c1af42f51ec1b59e))

## [2.3.101](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.100...v2.3.101) (2024-05-02)


### Bug Fixes

* **deps:** lock file maintenance ([bb81526](https://github.com/scratchfoundation/scratch-storage/commit/bb81526edb1b9994918ec90871e64d08bc526436))

## [2.3.100](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.99...v2.3.100) (2024-05-02)


### Bug Fixes

* **deps:** lock file maintenance ([16e23a5](https://github.com/scratchfoundation/scratch-storage/commit/16e23a58b3bb3ffd1c0344e34d728651be2af09f))

## [2.3.99](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.98...v2.3.99) (2024-05-01)


### Bug Fixes

* **deps:** lock file maintenance ([9c2c31b](https://github.com/scratchfoundation/scratch-storage/commit/9c2c31b9cc1cba49e7645290399d92ec937857fb))

## [2.3.98](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.97...v2.3.98) (2024-04-30)


### Bug Fixes

* **deps:** lock file maintenance ([5f47f25](https://github.com/scratchfoundation/scratch-storage/commit/5f47f255231460b5366a1188b4693357e608e0d4))

## [2.3.97](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.96...v2.3.97) (2024-04-30)


### Bug Fixes

* **deps:** lock file maintenance ([042b38e](https://github.com/scratchfoundation/scratch-storage/commit/042b38ed624a97815b1b891bf1ab617d612b12ee))

## [2.3.96](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.95...v2.3.96) (2024-04-29)


### Bug Fixes

* **deps:** lock file maintenance ([8e37a52](https://github.com/scratchfoundation/scratch-storage/commit/8e37a521eba536204dfb76ac8900f81930b6eab9))

## [2.3.95](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.94...v2.3.95) (2024-04-28)


### Bug Fixes

* **deps:** lock file maintenance ([8cf4636](https://github.com/scratchfoundation/scratch-storage/commit/8cf46368f792a32f1079a608cfba5e8a8754eb74))

## [2.3.94](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.93...v2.3.94) (2024-04-27)


### Bug Fixes

* **deps:** lock file maintenance ([5b86567](https://github.com/scratchfoundation/scratch-storage/commit/5b86567dad0150d424234ca392ce1b3a58230217))

## [2.3.93](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.92...v2.3.93) (2024-04-26)


### Bug Fixes

* **deps:** lock file maintenance ([1583fab](https://github.com/scratchfoundation/scratch-storage/commit/1583fabdf237047d3741837cc7efaddd3154ef64))

## [2.3.92](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.91...v2.3.92) (2024-04-25)


### Bug Fixes

* **deps:** lock file maintenance ([910ff65](https://github.com/scratchfoundation/scratch-storage/commit/910ff65021fab241dd1291012cbff3c78a0156e4))

## [2.3.91](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.90...v2.3.91) (2024-04-24)


### Bug Fixes

* **deps:** lock file maintenance ([2205b8a](https://github.com/scratchfoundation/scratch-storage/commit/2205b8a1f69d11f77c182903b822210633173788))

## [2.3.90](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.89...v2.3.90) (2024-04-23)


### Bug Fixes

* **deps:** lock file maintenance ([391fa34](https://github.com/scratchfoundation/scratch-storage/commit/391fa348fafc61ea9fa99a698a2d87eb5371ab86))

## [2.3.89](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.88...v2.3.89) (2024-04-22)


### Bug Fixes

* **deps:** lock file maintenance ([dc11291](https://github.com/scratchfoundation/scratch-storage/commit/dc112919ff0d5bb11fd2f0e920e76c8f309b75c6))

## [2.3.88](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.87...v2.3.88) (2024-04-20)


### Bug Fixes

* **deps:** lock file maintenance ([833e2b7](https://github.com/scratchfoundation/scratch-storage/commit/833e2b73b23d0887a50651a89990b6028fc4f06d))

## [2.3.87](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.86...v2.3.87) (2024-04-19)


### Bug Fixes

* **deps:** lock file maintenance ([c24f135](https://github.com/scratchfoundation/scratch-storage/commit/c24f1359ef9975f4e9ff741448bb57d9c8ff4047))

## [2.3.86](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.85...v2.3.86) (2024-04-19)


### Bug Fixes

* **deps:** lock file maintenance ([e51d6a5](https://github.com/scratchfoundation/scratch-storage/commit/e51d6a59313b19c2edca6a6ebe666e648502e3ac))

## [2.3.85](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.84...v2.3.85) (2024-04-18)


### Bug Fixes

* **deps:** lock file maintenance ([5c3788d](https://github.com/scratchfoundation/scratch-storage/commit/5c3788d97581bfddd17c2631bed1a696ddd63f25))

## [2.3.84](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.83...v2.3.84) (2024-04-17)


### Bug Fixes

* **deps:** lock file maintenance ([ea08f09](https://github.com/scratchfoundation/scratch-storage/commit/ea08f09874da98029e91a25bf1291d421929d475))

## [2.3.83](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.82...v2.3.83) (2024-04-16)


### Bug Fixes

* **deps:** lock file maintenance ([22aa8a4](https://github.com/scratchfoundation/scratch-storage/commit/22aa8a4571c449f2b01bab1a6e9e11df1d19e206))

## [2.3.82](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.81...v2.3.82) (2024-04-15)


### Bug Fixes

* **deps:** lock file maintenance ([199ce00](https://github.com/scratchfoundation/scratch-storage/commit/199ce00635dd8a433c260854c7f3ecf27c4f231a))

## [2.3.81](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.80...v2.3.81) (2024-04-13)


### Bug Fixes

* **deps:** lock file maintenance ([ef351d8](https://github.com/scratchfoundation/scratch-storage/commit/ef351d8c09a45c96faffa0bd475a6245caf3f215))

## [2.3.80](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.79...v2.3.80) (2024-04-13)


### Bug Fixes

* **deps:** lock file maintenance ([3934dfb](https://github.com/scratchfoundation/scratch-storage/commit/3934dfb38916c74355e6c52240a95160b3defcc9))

## [2.3.79](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.78...v2.3.79) (2024-04-12)


### Bug Fixes

* **deps:** lock file maintenance ([03d71ee](https://github.com/scratchfoundation/scratch-storage/commit/03d71ee543cf6bef6d05b18d1b406c1284bea9ae))

## [2.3.78](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.77...v2.3.78) (2024-04-11)


### Bug Fixes

* **deps:** lock file maintenance ([a6f581d](https://github.com/scratchfoundation/scratch-storage/commit/a6f581dc883f1107e01bb225da090b8726553d02))

## [2.3.77](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.76...v2.3.77) (2024-04-10)


### Bug Fixes

* **deps:** lock file maintenance ([6a6e971](https://github.com/scratchfoundation/scratch-storage/commit/6a6e9715700e2cb719006780baad61a0f368fe8b))

## [2.3.76](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.75...v2.3.76) (2024-04-09)


### Bug Fixes

* **deps:** lock file maintenance ([b81f131](https://github.com/scratchfoundation/scratch-storage/commit/b81f1319ce97fd9a83a76d87798390a3ce7a3cf7))

## [2.3.75](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.74...v2.3.75) (2024-04-06)


### Bug Fixes

* **deps:** lock file maintenance ([87a4b3b](https://github.com/scratchfoundation/scratch-storage/commit/87a4b3b5823bef1eadf7f1b99858e4764823a4be))

## [2.3.74](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.73...v2.3.74) (2024-04-06)


### Bug Fixes

* **deps:** lock file maintenance ([d90dfe3](https://github.com/scratchfoundation/scratch-storage/commit/d90dfe323e1e2e0ee8bc99eb19a8111b63d42322))

## [2.3.73](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.72...v2.3.73) (2024-04-04)


### Bug Fixes

* **deps:** lock file maintenance ([864dbdc](https://github.com/scratchfoundation/scratch-storage/commit/864dbdc0f9674b257526c98fcb78a35398d8be54))

## [2.3.72](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.71...v2.3.72) (2024-04-04)


### Bug Fixes

* **deps:** lock file maintenance ([873d857](https://github.com/scratchfoundation/scratch-storage/commit/873d857e6b4b7ee644729953fb639afacdd05c8e))

## [2.3.71](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.70...v2.3.71) (2024-04-03)


### Bug Fixes

* **deps:** lock file maintenance ([c35738d](https://github.com/scratchfoundation/scratch-storage/commit/c35738d8417f62032fc3342bdbf1f4b6d4908a93))

## [2.3.70](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.69...v2.3.70) (2024-04-02)


### Bug Fixes

* **deps:** lock file maintenance ([7f8e4bc](https://github.com/scratchfoundation/scratch-storage/commit/7f8e4bcb81f525e49b9a64e5bbf4deef34e42d59))

## [2.3.69](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.68...v2.3.69) (2024-04-02)


### Bug Fixes

* **deps:** lock file maintenance ([506a7ce](https://github.com/scratchfoundation/scratch-storage/commit/506a7ceaa68b070bb726687b3525043d20adb19f))

## [2.3.68](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.67...v2.3.68) (2024-03-31)


### Bug Fixes

* **deps:** lock file maintenance ([9141539](https://github.com/scratchfoundation/scratch-storage/commit/91415396ef2da084c1e1618d9a8317a462d2cb9b))

## [2.3.67](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.66...v2.3.67) (2024-03-30)


### Bug Fixes

* **deps:** lock file maintenance ([60bb749](https://github.com/scratchfoundation/scratch-storage/commit/60bb749ee13213a9b17ae4c1b973349dfcbf5d8f))

## [2.3.66](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.65...v2.3.66) (2024-03-30)


### Bug Fixes

* **deps:** lock file maintenance ([340dd3d](https://github.com/scratchfoundation/scratch-storage/commit/340dd3dd35c22d951d6b7931ca942ca2f36002fe))

## [2.3.65](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.64...v2.3.65) (2024-03-29)


### Bug Fixes

* **deps:** lock file maintenance ([47ba7c3](https://github.com/scratchfoundation/scratch-storage/commit/47ba7c39f6b09febebe39056fe66d7c5d3c2ff44))

## [2.3.64](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.63...v2.3.64) (2024-03-29)


### Bug Fixes

* **deps:** lock file maintenance ([ef57330](https://github.com/scratchfoundation/scratch-storage/commit/ef57330f792425ded463efc744dd60fa35cb3597))

## [2.3.63](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.62...v2.3.63) (2024-03-28)


### Bug Fixes

* **deps:** lock file maintenance ([e2a980a](https://github.com/scratchfoundation/scratch-storage/commit/e2a980a27f2c6c470e37be84b88e75dd6f7b4af2))

## [2.3.62](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.61...v2.3.62) (2024-03-28)


### Bug Fixes

* **deps:** lock file maintenance ([036715a](https://github.com/scratchfoundation/scratch-storage/commit/036715a5ce261dde9fe15558bee558c28841ea3a))

## [2.3.61](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.60...v2.3.61) (2024-03-26)


### Bug Fixes

* **deps:** lock file maintenance ([8f78222](https://github.com/scratchfoundation/scratch-storage/commit/8f782228a52c05998863630eeb472f9aa93497fc))

## [2.3.60](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.59...v2.3.60) (2024-03-26)


### Bug Fixes

* **deps:** lock file maintenance ([ff8ba11](https://github.com/scratchfoundation/scratch-storage/commit/ff8ba110ecbba92dc41edbeeb5607c9693d2bea6))

## [2.3.59](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.58...v2.3.59) (2024-03-25)


### Bug Fixes

* **deps:** lock file maintenance ([911a44a](https://github.com/scratchfoundation/scratch-storage/commit/911a44a79ffa581f80dcda9820eac19ffe6c7167))

## [2.3.58](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.57...v2.3.58) (2024-03-23)


### Bug Fixes

* **deps:** lock file maintenance ([4bc365f](https://github.com/scratchfoundation/scratch-storage/commit/4bc365fb21c8d1b593b87faf0b6e7b65e399fa9e))

## [2.3.57](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.56...v2.3.57) (2024-03-23)


### Bug Fixes

* **deps:** lock file maintenance ([62bcf03](https://github.com/scratchfoundation/scratch-storage/commit/62bcf03557a1129b5ccf94005cdba21d339fb800))

## [2.3.56](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.55...v2.3.56) (2024-03-22)


### Bug Fixes

* **deps:** lock file maintenance ([3383b0b](https://github.com/scratchfoundation/scratch-storage/commit/3383b0b89f6dbc9642ce4c89939189506df0e3a4))

## [2.3.55](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.54...v2.3.55) (2024-03-22)


### Bug Fixes

* **deps:** lock file maintenance ([4a7e7a1](https://github.com/scratchfoundation/scratch-storage/commit/4a7e7a18585b38c04fe7b5ebd3eba5c9572680c5))

## [2.3.54](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.53...v2.3.54) (2024-03-21)


### Bug Fixes

* **deps:** lock file maintenance ([df1e81c](https://github.com/scratchfoundation/scratch-storage/commit/df1e81c717186b12c068d2b151d11f5586c446be))

## [2.3.53](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.52...v2.3.53) (2024-03-21)


### Bug Fixes

* **deps:** lock file maintenance ([b054528](https://github.com/scratchfoundation/scratch-storage/commit/b0545280fb52ae098c6430b0e3b1b5e7d0b6ee3d))

## [2.3.52](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.51...v2.3.52) (2024-03-20)


### Bug Fixes

* **deps:** lock file maintenance ([7892613](https://github.com/scratchfoundation/scratch-storage/commit/7892613abb9728b61fe78a54639acce7a0ccf198))

## [2.3.51](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.50...v2.3.51) (2024-03-20)


### Bug Fixes

* **deps:** lock file maintenance ([eceef25](https://github.com/scratchfoundation/scratch-storage/commit/eceef25aa35848afc84ead747f50e3524a6ff0d5))

## [2.3.50](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.49...v2.3.50) (2024-03-19)


### Bug Fixes

* **deps:** lock file maintenance ([7feadb8](https://github.com/scratchfoundation/scratch-storage/commit/7feadb85173f8ea77d623d83b698962c3717782f))

## [2.3.49](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.48...v2.3.49) (2024-03-19)


### Bug Fixes

* **deps:** lock file maintenance ([04402ab](https://github.com/scratchfoundation/scratch-storage/commit/04402abaa7ef63b0e6b8251a7badbd3b43724f6c))

## [2.3.48](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.47...v2.3.48) (2024-03-18)


### Bug Fixes

* **deps:** lock file maintenance ([0ff0fb7](https://github.com/scratchfoundation/scratch-storage/commit/0ff0fb71dde420e919c08c312bdd32c66cf2375a))

## [2.3.47](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.46...v2.3.47) (2024-03-17)


### Bug Fixes

* **deps:** lock file maintenance ([93da28a](https://github.com/scratchfoundation/scratch-storage/commit/93da28ab41a213b04f01c9839fa52a017a9bd057))

## [2.3.46](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.45...v2.3.46) (2024-03-16)


### Bug Fixes

* **deps:** lock file maintenance ([769a6ef](https://github.com/scratchfoundation/scratch-storage/commit/769a6efd1a5d1c9cc697a0f02d4879a3cb06510b))

## [2.3.45](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.44...v2.3.45) (2024-03-15)


### Bug Fixes

* **deps:** lock file maintenance ([8edda47](https://github.com/scratchfoundation/scratch-storage/commit/8edda47694df8783d4c3e1d0ea7ed5814744bda1))

## [2.3.44](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.43...v2.3.44) (2024-03-15)


### Bug Fixes

* **deps:** lock file maintenance ([e51a340](https://github.com/scratchfoundation/scratch-storage/commit/e51a3403c209c4829287b393d053e0b10ba20e67))

## [2.3.43](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.42...v2.3.43) (2024-03-14)


### Bug Fixes

* **deps:** lock file maintenance ([a57af39](https://github.com/scratchfoundation/scratch-storage/commit/a57af399d372ee7eccd5d438ed418b7466ade21a))

## [2.3.42](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.41...v2.3.42) (2024-03-13)


### Bug Fixes

* **deps:** lock file maintenance ([f507b02](https://github.com/scratchfoundation/scratch-storage/commit/f507b02dfaada6c14eeec5266478507428d9160b))

## [2.3.41](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.40...v2.3.41) (2024-03-12)


### Bug Fixes

* **deps:** lock file maintenance ([3c124d6](https://github.com/scratchfoundation/scratch-storage/commit/3c124d62e7062dd53b3609910ecd56910e1d6440))

## [2.3.40](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.39...v2.3.40) (2024-03-11)


### Bug Fixes

* **deps:** lock file maintenance ([14f32d6](https://github.com/scratchfoundation/scratch-storage/commit/14f32d646debe4f2dd8791318fad2da57898b484))

## [2.3.39](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.38...v2.3.39) (2024-03-10)


### Bug Fixes

* **deps:** lock file maintenance ([f36218f](https://github.com/scratchfoundation/scratch-storage/commit/f36218f4983a9fa32a309228f920f12d4e557e26))

## [2.3.38](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.37...v2.3.38) (2024-03-09)


### Bug Fixes

* **deps:** lock file maintenance ([2d7c80e](https://github.com/scratchfoundation/scratch-storage/commit/2d7c80e7287cf2a5b365a9cc178cf5a7efc1a13f))

## [2.3.37](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.36...v2.3.37) (2024-03-09)


### Bug Fixes

* **deps:** lock file maintenance ([a774e54](https://github.com/scratchfoundation/scratch-storage/commit/a774e545cd79cc6227040f024d7d5e80f5954745))

## [2.3.36](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.35...v2.3.36) (2024-03-08)


### Bug Fixes

* **deps:** lock file maintenance ([1708420](https://github.com/scratchfoundation/scratch-storage/commit/1708420cd192929d461f2f6f081201e39e97956e))

## [2.3.35](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.34...v2.3.35) (2024-03-08)


### Bug Fixes

* **deps:** lock file maintenance ([bfba892](https://github.com/scratchfoundation/scratch-storage/commit/bfba8921d92faa29edd07d1a7be72410819a16b0))

## [2.3.34](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.33...v2.3.34) (2024-03-07)


### Bug Fixes

* **deps:** lock file maintenance ([421d14c](https://github.com/scratchfoundation/scratch-storage/commit/421d14c8b2d0d547a140e2ac816ecde4142da863))

## [2.3.33](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.32...v2.3.33) (2024-03-06)


### Bug Fixes

* **deps:** lock file maintenance ([f4e49c5](https://github.com/scratchfoundation/scratch-storage/commit/f4e49c5c6a5c7dd67ce3b65face887105dc5d481))

## [2.3.32](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.31...v2.3.32) (2024-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([37e319b](https://github.com/scratchfoundation/scratch-storage/commit/37e319bab9752d49902772b73f74f7938e2e4503))

## [2.3.31](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.30...v2.3.31) (2024-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([04f0b06](https://github.com/scratchfoundation/scratch-storage/commit/04f0b06a56c497be116a8f98d6aff52416c5a239))

## [2.3.30](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.29...v2.3.30) (2024-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([f4ab41d](https://github.com/scratchfoundation/scratch-storage/commit/f4ab41db6f7ab64633d4f84721f69601a2209a88))

## [2.3.29](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.28...v2.3.29) (2024-03-04)


### Bug Fixes

* **deps:** lock file maintenance ([a906d89](https://github.com/scratchfoundation/scratch-storage/commit/a906d899422e5dfc66f318696de2c15ec5a58b2b))

## [2.3.28](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.27...v2.3.28) (2024-03-03)


### Bug Fixes

* **deps:** lock file maintenance ([954e0c9](https://github.com/scratchfoundation/scratch-storage/commit/954e0c9687bbe0de08a39206d16a2757f6d4bd08))

## [2.3.27](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.26...v2.3.27) (2024-03-02)


### Bug Fixes

* **deps:** lock file maintenance ([06f1b71](https://github.com/scratchfoundation/scratch-storage/commit/06f1b715b5df70e9cfdbf068527772fd691fb4c7))

## [2.3.26](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.25...v2.3.26) (2024-03-01)


### Bug Fixes

* **deps:** lock file maintenance ([34bf90a](https://github.com/scratchfoundation/scratch-storage/commit/34bf90a33a3b603534168414520d46c66cd347da))

## [2.3.25](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.24...v2.3.25) (2024-03-01)


### Bug Fixes

* **deps:** lock file maintenance ([f1b0014](https://github.com/scratchfoundation/scratch-storage/commit/f1b00148abb425314db7b23ebb1d36b4a09ba1bb))

## [2.3.24](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.23...v2.3.24) (2024-03-01)


### Bug Fixes

* **deps:** lock file maintenance ([c3fac02](https://github.com/scratchfoundation/scratch-storage/commit/c3fac0246d355350e1c8232040b7cb1a23a98163))

## [2.3.23](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.22...v2.3.23) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([89843cc](https://github.com/scratchfoundation/scratch-storage/commit/89843cc306419711bebadd6995fc6955cb445f2e))

## [2.3.22](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.21...v2.3.22) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([9538bac](https://github.com/scratchfoundation/scratch-storage/commit/9538bacc199e146241b4f9a06717336d42ee7073))

## [2.3.21](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.20...v2.3.21) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([0ef5d02](https://github.com/scratchfoundation/scratch-storage/commit/0ef5d020ed68e872fafb951fe3a7a47109e83f6c))

## [2.3.20](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.19...v2.3.20) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([7f623de](https://github.com/scratchfoundation/scratch-storage/commit/7f623de658a65647b498d677ff8ed1d65486fd74))

## [2.3.19](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.18...v2.3.19) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([b99988e](https://github.com/scratchfoundation/scratch-storage/commit/b99988ec2a19c9a44ef86307824d9579ec6cf180))

## [2.3.18](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.17...v2.3.18) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([2e98b05](https://github.com/scratchfoundation/scratch-storage/commit/2e98b052d3f0f748cdf99ef608ae0c99072681d0))

## [2.3.17](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.16...v2.3.17) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([022fb64](https://github.com/scratchfoundation/scratch-storage/commit/022fb64b6d091b2cba0e024c33eca9f96bb420e4))

## [2.3.16](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.15...v2.3.16) (2024-02-28)


### Bug Fixes

* **deps:** lock file maintenance ([d5ed839](https://github.com/scratchfoundation/scratch-storage/commit/d5ed839f4ce825a9b800f07071bc19f2e71c5aa2))

## [2.3.15](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.14...v2.3.15) (2024-02-28)


### Bug Fixes

* **deps:** lock file maintenance ([b0d19af](https://github.com/scratchfoundation/scratch-storage/commit/b0d19af2acb3a33bf0e238bbfbe9ea75a1105816))

## [2.3.14](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.13...v2.3.14) (2024-02-28)


### Bug Fixes

* **deps:** lock file maintenance ([0bf14c4](https://github.com/scratchfoundation/scratch-storage/commit/0bf14c4b269e8f5416fbf3d7a35782859d2e7a33))

## [2.3.13](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.12...v2.3.13) (2024-02-28)


### Bug Fixes

* **deps:** lock file maintenance ([a779fa1](https://github.com/scratchfoundation/scratch-storage/commit/a779fa1793184798b2bf77f343c11a263dab5c8e))

## [2.3.12](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.11...v2.3.12) (2024-02-28)


### Bug Fixes

* **deps:** lock file maintenance ([96c4015](https://github.com/scratchfoundation/scratch-storage/commit/96c401570033b8d07d58a6cef9d2997ccb349aa7))

## [2.3.11](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.10...v2.3.11) (2024-02-27)


### Bug Fixes

* **deps:** lock file maintenance ([e37617a](https://github.com/scratchfoundation/scratch-storage/commit/e37617a18e2aedffb7c532e2132d051d87df31fd))

## [2.3.10](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.9...v2.3.10) (2024-02-27)


### Bug Fixes

* **deps:** lock file maintenance ([116c64d](https://github.com/scratchfoundation/scratch-storage/commit/116c64d0c97cb9a539e288526b48e302f14f4b76))

## [2.3.9](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.8...v2.3.9) (2024-02-26)


### Bug Fixes

* **deps:** lock file maintenance ([bf2bae0](https://github.com/scratchfoundation/scratch-storage/commit/bf2bae02a11c674555a81acff8efdcf8fc81954e))

## [2.3.8](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.7...v2.3.8) (2024-02-26)


### Bug Fixes

* **deps:** lock file maintenance ([1a5e495](https://github.com/scratchfoundation/scratch-storage/commit/1a5e495c7f4028c3eee871767844283c48252c44))

## [2.3.7](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.6...v2.3.7) (2024-02-26)


### Bug Fixes

* **deps:** lock file maintenance ([c3a567d](https://github.com/scratchfoundation/scratch-storage/commit/c3a567de57a5b82e28362984f8a42ab50da9ca0f))

## [2.3.6](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.5...v2.3.6) (2024-02-24)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.14 ([adc5c57](https://github.com/scratchfoundation/scratch-storage/commit/adc5c57b9e65b172e7fef231a3d61a04779a3ca5))

## [2.3.5](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.4...v2.3.5) (2024-02-22)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.13 ([58a059b](https://github.com/scratchfoundation/scratch-storage/commit/58a059ba19e8051820f059f2e0d4fcb34e7e98ae))

## [2.3.4](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.3...v2.3.4) (2024-02-22)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.12 ([2a14b1d](https://github.com/scratchfoundation/scratch-storage/commit/2a14b1d3b9cdce993bde0fddd4e04306c82cf8ae))

## [2.3.3](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.2...v2.3.3) (2024-02-22)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.11 ([3d12e5d](https://github.com/scratchfoundation/scratch-storage/commit/3d12e5d60595254918f926f510623ed4ea2cc2b5))

## [2.3.2](https://github.com/scratchfoundation/scratch-storage/compare/v2.3.1...v2.3.2) (2024-02-21)


### Bug Fixes

* **deps:** lock file maintenance ([ca51101](https://github.com/scratchfoundation/scratch-storage/commit/ca511019f5caddf8bc540a12ebfc808bb75973d0))
